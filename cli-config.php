<?php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

/*'url' => "oci8://its_dev:its_pwd@10.1.106.252/uat?charset=utf8"*/

$dbParam = array(
    'url' => "oci8://its_prod:pwd_its@10.1.5.148/interndb?charset=utf8"
);

$paths = array(__DIR__ . "/app/entities");
$proxiesPath = __DIR__ . "/bootstrap/proxies";
$config = Setup::createAnnotationMetadataConfiguration($paths, false, null, null, false);
$config->setProxyDir($proxiesPath);
$config->setProxyNamespace('GeneratedProxies');

$conn = \Doctrine\DBAL\DriverManager::getConnection($dbParam, $config);

$entityManager = EntityManager::create($dbParam, $config);

/*
$cache = new \Doctrine\Common\Cache\ArrayCache;     
        
$config = new Configuration;
$config->setMetadataCacheImpl($cache);
$driverImpl = $config->newDefaultAnnotationDriver(__DIR__ . '/App/Entities');
$config->setMetadataDriverImpl($driverImpl);
$config->setQueryCacheImpl($cache);
$config->setProxyDir(__DIR__ . '/App/Proxies');
$config->setProxyNamespace('Proxies');
$config->setAutoGenerateProxyClasses(true);

        
$entityManager = EntityManager::create($dbParam,$config);
*/

return ConsoleRunner::createHelperSet($entityManager);
