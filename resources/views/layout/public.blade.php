<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบบริหารจัดการ Swu-Internship @yield('title')</title>
    <link rel="shortcut icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    <link rel="icon" href="@relative('assets/images/favicon.ico')" type="image/x-icon">
    
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="@relative('assets/fonts/fontawesome/css/fontawesome-all.min.css')">
    <!-- animation css -->
    <link rel="stylesheet" href="@relative('assets/plugins/animation/css/animate.min.css')">
    <!-- vendor css -->
    <link rel="stylesheet" href="@relative('assets/css/style.css')">
    
    @yield('style')
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <input type="hidden" name="current-path" id="current-path" value="@relative('')">

    @yield('content')    

    <script src="@relative('assets/js/vendor-all.min.js')"></script>
    <script src="@relative('assets/plugins/bootstrap/js/bootstrap.min.js')"></script>
    <script src="@relative('assets/plugins/jquery-validation/js/jquery.validate.min.js')"></script>
    <script src="@relative('assets/config/default.jquery-validation.js')"></script>
    @yield('script')
</body>
</html>