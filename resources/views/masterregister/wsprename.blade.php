@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection



@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">คำนำหน้าชื่อ</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลพื้นฐานงานทะเบียน</a></li>
    <li class="breadcrumb-item"><a href="#!">คำนำหน้าชื่อ</a></li>
</ul>
@endsection


@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-body">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:20%;">รหัส</th>
                            <th style="text-align:left;width:40%;">ชื่อคำนำหน้าไทย</th>
                            <th style="text-align:left;width:40%;">ชื่อคำนำหน้าอังกฤษ</th>                                                                             
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {            
            ajax: "{{api('WSPrename')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'prenameCd','width':'20%'},
                {'data':'prenameLnameTh','width':'40%'},
                {'data':'prenameLnameEng','width':'40%'}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0],
                },
            ],      
            buttons: [
                {
                    extend: 'print',
                    title: "คำนำหน้าชื่อ",
                    text: "<i class=\"fa fa-print\"></i> คำนำหน้าชื่อ",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "คำนำหน้าชื่อ",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],     
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",   
            'processing': true         
        });                            
    });
</script>
@endsection
