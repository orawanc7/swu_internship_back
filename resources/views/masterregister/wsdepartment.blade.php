@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection


@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">หน่วยงาน</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลพื้นฐานงานทะเบียน</a></li>
    <li class="breadcrumb-item"><a href="#!">หน่วยงาน</a></li>
</ul>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">        
            <div class="card-body">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:text-center;width:20%">รหัส</th>
                            <th style="text-align:text-left;width:35%">ชื่อหน่วยงานไทย</th>
                            <th style="text-align:text-left;width:35%">ชื่อหน่วยงานอังกฤษ</th>                                
                            <th style="text-align:text-center;width:10%">สถานะ</th>                        
                        </tr>
                    </thead>
                    <tbody>                                     
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>       
    var table;
    $(document).ready(function () {                
        table = $('#table').DataTable( {                        
            ajax: "{{api('WSDepartment')}}",
            type:'GET',
            dataType:'json',            
            columns:[
                {'data':'deptCd','width':'20%'},
                {'data':'deptLnameTh','width':'35%'},
                {'data':'deptLnameEng','width':'35%'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check text-success\"></i>";
                    } else {
                        return "";
                    }
                }},
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],
                },
            ],
            buttons: [
                {
                    extend: 'print',
                    title: "หน่วยงาน",
                    text: "<i class=\"fa fa-print\"></i> พิมพ์",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "หน่วยงาน",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            'processing': true         
        });

        //$( table.table().container() ).removeClass( 'form-inline' );
                    
    });
</script>
@endsection
