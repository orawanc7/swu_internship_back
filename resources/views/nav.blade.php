<!-- [ navigation menu ] start -->
<nav class="pcoded-navbar menu-light">
	<div class="navbar-wrapper">
		<div class="navbar-brand header-logo">
		<a href="index.html" class="b-brand">
			<img src="@relative('assets/images/logo-light.png')" alt="" class="logo images">		
			<img src="@relative('assets/images/logo-icon.svg')" alt="" class="logo-thumb images">	
		</a>
			<a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
		</div>
		<div class="navbar-content scroll-div">
			<ul class="nav pcoded-inner-navbar">
				<li class="nav-item pcoded-menu-caption">
					<label>ระบบบริหารจัดการ Swu-Internship</label>
				</li>
				<li data-username="Home" class="nav-item"><a href="{{ route('home') }}" class="nav-link"><span class="pcoded-micon"><i class="fa fa-home"></i></span><span class="pcoded-mtext">หน้าหลัก</span></a></li>
				<li data-username="Register" class="nav-item pcoded-hasmenu">
					<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="fa fa-box"></i></span><span class="pcoded-mtext">ข้อมูลพื้นฐานงานทะเบียน</span></a>
					<ul class="pcoded-submenu">
						<li class=""><a href="{{ route('wsdepartment') }}" class="">หน่วยงาน</a></li>
						<li class=""><a href="{{ route('wsmajor') }}" class="">สาขาวิชาเอก</a></li>
						<li class=""><a href="{{ route('wsprename') }}" class="">คำนำหน้าชื่อ</a></li>
						<li class=""><a href="{{ route('wslevelgroup') }}" class="">กลุ่มระดับการศึกษา</a></li>
						<li class=""><a href="{{ route('wsdegree') }}" class="">วุฒิการศึกษา</a></li>
					</ul>
				</li>
				
				<li data-username="Its" class="nav-item pcoded-hasmenu">
					<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="fa fa-box"></i></span><span class="pcoded-mtext">ข้อมูลพื้นฐานระบบนิเทศ</span></a>
					<ul class="pcoded-submenu">
						<li class=""><a href="{{route('persongroup')}}" class="">กลุ่มบุคลากร</a></li>
						<li class=""><a href="{{route('persontype')}}" class="">ประเภทบุคคลากร</a></li>						
						<li class=""><a href="{{route('school')}}" class="">โรงเรียน</a></li>	
					</ul>
				</li>

				<li data-username="Its" class="nav-item pcoded-hasmenu">
					<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="fa fa-box"></i></span><span class="pcoded-mtext">ข้อมูลเอกสารดาวน์โหลด</span></a>
					<ul class="pcoded-submenu">
						<li class=""><a href="{{route('docgroupdownload')}}" class="">กลุ่มเอกสารดาวน์โหลด</a></li>
						<li class=""><a href="{{route('docdownload')}}" class="">เอกสารดาวน์โหลด</a></li>												
					</ul>
				</li>

				<li data-username="Document" class="nav-item pcoded-hasmenu">
					<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="fa fa-box"></i></span><span class="pcoded-mtext">ข้อมูลเอกสารและแบบฟอร์ม</span></a>
					<ul class="pcoded-submenu">
						<li class=""><a href="{{route('documenttype')}}" class="">ประเภทเอกสาร</a></li>
						<li class=""><a href="{{route('document')}}" class="">เอกสาร</a></li>
						<li class=""><a href="{{route('form')}}" class="">แบบฟอร์ม</a></li>																					
					</ul>
				</li>

				<li data-username="Preprocess" class="nav-item pcoded-hasmenu">
					<a href="#!" class="nav-link"><span class="pcoded-micon"><i class="fa fa-box"></i></span><span class="pcoded-mtext">ข้อมูลก่อนการฝึกสอน</span></a>
					<ul class="pcoded-submenu">
						<li class=""><a href="{{route('schdround')}}" class="">รอบการฝึกปฏิบัติการสอน</a></li>
						{{-- <li class=""><a href="{{route('schdschool')}}" class="">กำหนดสถานศึกษาตามรอบ</a></li>
						<li class=""><a href="" class="">นิสิตแต่ละโรงเรียนตามสาขา</a></li>						
						<li class=""><a href="{{route('schdactivity')}}" class="">กิจกรรม</a></li>																										 --}}
					</ul>
				</li>
				
			</ul>
		</div>
	</div>
</nav>
<!-- [ navigation menu ] end -->