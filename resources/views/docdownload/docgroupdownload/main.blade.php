@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">กลุ่มเอกสาร</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลเอกสารดาวน์โหลด</a></li>
    <li class="breadcrumb-item"><a href="#!">กลุ่มเอกสาร</a></li>
</ul>
@endsection

@section('content')
<ul class="nav nav-tabs" id="tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="search" aria-selected="true"><i class="fa fa-search"></i> ค้นหา</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="save-tab" data-toggle="tab" href="#save" role="tab" aria-controls="save" aria-selected="false"><i class="fa fa-save"></i> บันทึก</a>
    </li>    
</ul>
<div class="tab-content" id="tabContent">
    <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="search-tab">
        @include('docdownload.docgroupdownload.search')
    </div>
    <div class="tab-pane fade" id="save" role="tabpanel" aria-labelledby="save-tab">
        @include('docdownload.docgroupdownload.save')
    </div>    
</div>
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>   
    var table;
    var frmObj;
    $(document).ready(function () {    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
                                    
        table = $('#table').DataTable( {            
            ajax: "{{api('DocGroupDownload')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'docGroupDownloadId'},
                {'data':'docGroupDownloadNameTh'},
                {'data':'docGroupDownloadNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check text-success\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'docGroupDownloadId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:DocGroupDownload.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:DocGroupDownload.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],                                                        
                },
                {
                    "className": "text-right", "targets": [4],
                }    
            ],                
            buttons: [
                {
                    text: "<i class=\"fa fa-plus\"></i>",
                    className: "btn btn-info",
                    action: function (e, dt, node, config) {
                        DocGroupDownload.add();
                    }
                },
                {
                    extend: 'print',
                    title: "กลุ่มเอกสาร",
                    text: "<i class=\"fa fa-print\"></i> พิมพ์",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "กลุ่มเอกสาร",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],     
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            'processing': true,
            'responsive': true,                
        });       
        frmObj = $('#frmSave').validate();
        
        $('#frmSave').submit(function (e) { 
            
            e.preventDefault();
                                    
            var frm = $(this);

            if (frmObj.valid()) {
                MessageConfirm.save(function() {
                    DocGroupDownload.save(frm);
                },function() {

                });
            }

        });

        $('#btnCancel').click(function(){           
            DocGroupDownload.clear();
        });

        $("#docGroupDownloadId").inputmask({
            mask: "9[9]",
            greedy: false 
        });

        $("#seqId").inputmask({
            mask: "9[9]",
            greedy: false 
        });
    });

    var DocGroupDownload = {
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#docGroupDownloadId').val("");
            frmObj.resetForm();
            $('#docGroupDownloadId').focus();            
        },
        add: function() {
            DocGroupDownload.clear();
            $('a[href="#save"]').tab('show');
            setTimeout(function() {
                $('#docGroupDownloadId').select();
            },300);
        },
        edit: function(id) {            
            $.ajax({
                url: "{{api('DocGroupDownload/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        DocGroupDownload.clear();

                        var data = response.data;
                        $('#docGroupDownloadId').val(data.docGroupDownloadId);
                        $('#docGroupDownloadNameTh').val(data.docGroupDownloadNameTh);
                        $('#docGroupDownloadNameEn').val(data.docGroupDownloadNameEn);                                                
                        $('#seqId').val(data.seqId);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        
                        $('a[href="#save"]').tab('show');
                        setTimeout(function() {
                            $('#docGroupDownloadNameTh').select();
                        },300);                        
                    } else {
                        Message.error(response);
                    }
                }
            });
        },
        delete: function(id) {             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            DocGroupDownload.clear();
                            table.ajax.reload();

                            $('#docGroupDownloadId').focus();
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
            
            
        },
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        //DocGroupDownload.clear();
                        table.ajax.reload();                        
                        $('#docGroupDownloadNameTh').focus();

                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
