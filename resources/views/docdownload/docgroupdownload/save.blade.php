<form id="frmSave" action="{{ api('DocGroupDownload/save')}}" method="post">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="docGroupDownloadId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="docGroupDownloadId" name="docGroupDownloadId" required autofocus autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <label for="docGroupDownloadNameTh" class="col-md-2 col-form-label mandatory">ชื่อกลุ่มเอกสาร (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docGroupDownloadNameTh" name="docGroupDownloadNameTh" required autocomplete="off">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="docGroupDownloadNameEn" class="col-md-2 col-form-label mandatory">ชื่อกลุ่มเอกสาร (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docGroupDownloadNameEn" name="docGroupDownloadNameEn" autocomplete="off">
                </div>                        
            </div>             

            <div class="form-group row">
                <label for="seqId" class="col-md-2 col-form-label mandatory">ลำดับ</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="seqId" name="seqId" autocomplete="off">
                </div>                        
            </div>
                     
            <div class="form-group row">
                <div class="col-md-3 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>