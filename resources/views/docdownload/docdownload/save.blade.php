<form id="frmSave" action="{{ api('DocDownload/save')}}" method="post" enctype="multipart/form-data">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="docDownloadId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docDownloadId" name="docDownloadId" required autofocus autocomplete="off" maxlength="3">
                </div>                
            </div>
            <div class="form-group row">
                <label for="docGroupDownloadId" class="col-md-2 col-form-label mandatory">กลุ่มเอกสาร</label>
                <div class="col-md-10">
                    <select name="docGroupDownloadId" id="docGroupDownloadId" class="form-control select2"></select>
                </div>
            </div>
            <div class="form-group row">
                <label for="docDownloadNameTh" class="col-md-2 col-form-label mandatory">ชื่อเอกสาร (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docDownloadNameTh" name="docDownloadNameTh" required autocomplete="off" maxlength="200">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="docDownloadNameTh" class="col-md-2 col-form-label mandatory">ชื่อเอกสาร (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docDownloadNameEn" name="docDownloadNameEn" autocomplete="off" maxlength="200">
                </div>                        
            </div>          
            
            <div class="form-group row">
                <label for="seqId" class="col-md-2 col-form-label mandatory">ลำดับ</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="seqId" name="seqId" autocomplete="off">
                </div>                        
            </div>

            <div class="form-group row">
                <label for="docDownloadNameTh" class="col-md-2 col-form-label">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" rows="5" class="form-control"></textarea>
                </div>                        
            </div>            
               
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>

            <div class="form-group row" id="fileUpload">
                <label for="docDownloadNameTh" class="col-md-2 col-form-label">ไฟล์เอกสาร</label>
                <div class="col-md-10">
                    <span id="spnFile"></span><input name="file" type="file" id="file"/>
                </div>                        
            </div>     
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>