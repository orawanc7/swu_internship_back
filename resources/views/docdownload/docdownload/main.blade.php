@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">เอกสารดาวน์โหลด</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลเอกสารดาวน์โหลด</a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลเอกสารดาวน์โหลด</a></li>
</ul>
@endsection

@section('content')
<ul class="nav nav-tabs" id="tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="search" aria-selected="true"><i class="fa fa-search"></i> ค้นหา</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="save-tab" data-toggle="tab" href="#save" role="tab" aria-controls="save" aria-selected="false"><i class="fa fa-save"></i> บันทึก</a>
    </li>    
</ul>
<div class="tab-content" id="tabContent">
    <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="search-tab">
        @include('docdownload.docdownload.search')
    </div>
    <div class="tab-pane fade" id="save" role="tabpanel" aria-labelledby="save-tab">
        @include('docdownload.docdownload.save')
    </div>    
</div>
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>   
    var table;
    var frmObj;
    $(document).ready(function () {    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        table = $('#table').DataTable( {            
            ajax: "{{api('DocDownload/getAll')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'docDownloadId'},
                {'data':'docDownloadNameTh'},
                {'data':'docGroupDownloadNameTh'},
                {'data':'activeFlag',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check text-success\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'docDownloadId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:DocDownload.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:DocDownload.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],                                                        
                },
                {
                    "className": "text-right", "targets": [4],
                },
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: 4 }                                
            ],                
            buttons: [
                {
                    text: "<i class=\"fa fa-plus\"></i>",
                    className: "btn btn-info",
                    action: function (e, dt, node, config) {
                        DocDownload.add();
                    }
                },
                {
                    extend: 'print',
                    title: "เอกสารดาวน์โหลด",
                    text: "<i class=\"fa fa-print\"></i> พิมพ์",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "เอกสารดาวน์โหลด",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],     
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            'processing': true,
            'responsive': true
        });       
        frmObj = $('#frmSave').validate();
        
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            var frm = $(this);

            if (frmObj.valid()) {
                MessageConfirm.save(function() {
                    DocDownload.save(frm);
                },function() {

                });
            }

        });

        $('#btnCancel').click(function(){           
            DocDownload.clear();
        });

        $("#docDownloadId").inputmask({
            // mask: "9[99]",
            // greedy: false 
        });

        $('#docGroupDownloadId').select2({
            width:"100%"
        });

        DocDownload.loadDocGroupDownload();
    });

    var DocDownload = {
        loadDocGroupDownload () {
            $('#docGroupDownloadId').empty();            

            $.getJSON("{{ api('DocGroupDownload' )}}", function(data){                         
                $("#docGroupDownloadId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#docGroupDownloadId").append("<option value=" + item.docGroupDownloadId + ">" + item.docGroupDownloadNameTh + "</option>"); 
                });
            });
        },
        add: function() {
            DocDownload.clear();
            $('a[href="#save"]').tab('show');
            setTimeout(function() {
                $('#docDownloadId').select();
            },300);
        },
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#docDownloadId').val("");
            frmObj.resetForm();
            
            $('#spnFile').empty();
            $('#docDownloadId').focus();            
        },
        edit: function(id) {            
            $.ajax({
                url: "{{api('DocDownload/get/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {                        
                        DocDownload.clear();

                        var data = response.data;
                        $('#docDownloadId').val(data.docDownloadId);
                        $('#docGroupDownloadId').val(data.docGroupDownloadId).trigger('change');
                        $('#docDownloadNameTh').val(data.docDownloadNameTh);
                        $('#docDownloadNameEn').val(data.docDownloadNameEn);
                        $('#remark').val(data.remark);

                        if (data.fileUrl) {
                            var url = "{{ api('') }}" + data.fileUrl;
                            
                            $('#spnFile').html(
                                "<a class=\"btn btn-sm bg-primary text-white\" href=\"" + url +"\"><i class=\"fa fa-file\"></i> เปิดไฟล์</a>"
                            );
                        }
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }                        
                        $('a[href="#save"]').tab('show');
                        
                        setTimeout(function() {
                            $('#docGroupDownloadId').select();
                        },300);     
                    }
                }
            });
        },
        delete: function(id) {             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            DocDownload.clear();
                            table.ajax.reload();

                            $('#docDownloadId').focus();
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
            
            
        },
        save: function(frm) {         
            var formData = new FormData();
            formData.append('file', $('#file')[0].files[0]);            
            formData.append('docDownloadId', $('#docDownloadId').val());
            formData.append('docGroupDownloadId', $('#docGroupDownloadId').val());
            formData.append('docDownloadNameTh', $('#docDownloadNameTh').val());
            formData.append('docDownloadNameEn', $('#docDownloadNameEn').val());
            formData.append('remark', $('#remark').val());
            formData.append('activeFlag', $('#activeFlag').val());
                        

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: formData,             
                processData: false,
                contentType: false, 
                success: function (response) {                    
                    if (response.status) {
                        DocDownload.clear();
                        table.ajax.reload();                        
                        $('#docGroupDownloadId').focus();

                        MessageNotify.saveSucess();
                    } else {
                        MessageNotify.error(response);
                    }
                }
            });

        }
    };
    
</script>
@endsection
