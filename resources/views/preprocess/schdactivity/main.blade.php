@extends('layout.app')

@section('style')
@include('plugin.datetime.css')
@include('plugin.jstree.css')
@include('plugin.multiselect.css')
<style>
.jstree-anchor {
    /*enable wrapping*/
    white-space : normal !important;
    /*ensure lower nodes move down*/
    height : auto !important;
    /*offset icon width*/
    padding-right : 24px;
}
</style>
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">กิจกรรม</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลก่อนการฝึกสอน</a></li>
    <li class="breadcrumb-item"><a href="{{ route('schdround')}}">รอบการฝึกปฏิบัติการสอน</a></li>
    <li class="breadcrumb-item"><a href="#!">กิจกรรม</a></li>
</ul>
@endsection

@section('content')
<div class="card">
    <div class="card-header bg-info">
        <div class="row">
            <div class="col-sm-6">
                <h5 class="text-white"><i class="fa fa-book"></i>  <span id="spnRoundInfo"></span> วิชา <span id="spnCourseInfo"></span></h5>

            </div>
            <div class="col-sm-6 text-right">                
                <button class="btn btn-sm btn-info" id="btnAdd" type="button"><i class="fa fa-plus"></i>เพิ่มกิจกรรม</button>
                <button class="btn btn-sm btn-danger" id="btnDelete" type="button"><i class="fa fa-trash"></i>ลบกิจกรรม</button>
            </div>
        </div>        
    </div>
    <div class="card-body">        
        <div class="dd" id="divActivity">
            <ol class="dd-list">
                
            </ol>
        </div>
    </div>
</div>

@include('preprocess.schdactivity.save')

@endsection

@section('script')
@include('plugin.datetime.js')
@include('plugin.jstree.js')
@include('plugin.multiselect.js')
<script>
    var frmObj;    
    $(document).ready(function () {
        $('#startDate').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            time:false
        });
        $('#endDate').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            time:false
        });

        $('#btnCancel').click(function(e){
            e.preventDefault();
            SchdActivity.clear();
        });

        $('#btnAdd').click(function (e) { 
            e.preventDefault();
            SchdActivity.add();
        });

        $('#btnDelete').click(function (e) { 
            e.preventDefault();       
            
            SchdActivity.delete();
            
        });

        $('#docId').select2({
            width:'100%'
        });
               
        
        $("#divActivity").jstree({
            "core" : {
                "check_callback" : true,          
                themes: { dots: true }      
            },
            "plugins" : [ "dnd","wholerow" ],            
        });

        $('#divActivity').bind('open_node.jstree', function () {
            let bar = $(this).find('.jstree-wholerow-clicked');
            bar.css('height',
                bar.parent().children('a.jstree-anchor').height() + 'px');
        });
        $('#divActivity').bind('hover_node.jstree', function () {
            var bar = $(this).find('.jstree-wholerow-hovered');
            bar.css('height',
                bar.parent().children('a.jstree-anchor').height() + 'px');
        });
        $('#divActivity').bind('select_node.jstree', function (evt, data) {
              var activityId = data.node.id;
              SchdActivity.edit(activityId);
        });

        $("#divActivity").bind("move_node.jstree", function(e, data) {                  
            var activityId = data.node.id;
            var refActivityId = (data.parent=="#")?"":data.parent;
            var activitySeqId = data.position+1;
            SchdActivity.update(activityId,activitySeqId,refActivityId);
        });
                
  
        SchdActivity.initMain();
        SchdActivity.loadDoc();
        SchdActivity.loadPersonType();
        SchdActivity.loadRound();
        SchdActivity.loadActivity();
    });
   
    var SchdActivity = {
        personType: [],
        loadDoc () {
            $('#docId').empty();            

            $.getJSON("{{ api('Doc' )}}", function(data){             
                $("#docId").append("<option value=''></option>");    
                $.each(data.data, function(index,item) {                                    
                    $("#docId").append("<option value=" + item.docId + ">" + item.docNameTh + "</option>"); 
                });
            });
        },
        loadPersonType () {
            $('#personType').empty();            

            $.getJSON("{{ api('PersonType' )}}", function(data){                                       
                $.each(data.data, function(index,item) {                                    
                    $("#personType").append("<option value=" + item.personTypeId + ">" + item.personTypeNameTh + "</option>"); 
                });

                
                $('#personType').multiSelect({
                    selectableHeader: "<div class='custom-header'>บุคลากรทั้งหมด</div>",
                    selectionHeader: "<div class='custom-header'>บุคลากรที่เข้าร่วมกิจกรรม</div>",
                    // selectableFooter: "<div class='custom-header'>Selectable footer</div>",
                    // selectionFooter: "<div class='custom-header'>Selection footer</div>"    
                    afterSelect:function(values) {
                        // var personTypeId = $('#personTypeId');
                        // if (personTypeId.val().length==0) {
                        //     personTypeId.val(values);
                        // } else {
                        //     personTypeId.val(personTypeId.val() + "," + values);
                        // }                        
                        SchdActivity.personType.push(values);                        
                    },  
                    afterDeselect: function (values) {
                        SchdActivity.personType.splice(SchdActivity.personType.indexOf(values), 1);                        
                    }              
                });
                

            });

            
        },
        initMain() {
            frmObj = $('#frmSave').validate();        
            $('#frmSave').submit(function (e) {                 
                e.preventDefault();                                        
                var frm = $(this);
                if (frmObj.valid()) {
                    MessageConfirm.save(function() {
                        SchdActivity.save(frm);
                    },function() {

                    });
                }
            });
        },
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#activityId').val("");
            $('#efactivityId').val("");
            $('#docId').val("").trigger('change');
            $('#personType').multiSelect('deselect_all');       
            SchdActivity.personType = [];     

            frmObj.resetForm();            
        },
        save: function(frm) {                  
            var personTypeId = $('#personTypeId');
            personTypeId.val("");
            
            $.each(SchdActivity.personType, function(index, item) {
                if (item.length>0) {                    
                    if (personTypeId.val().length==0) {
                        personTypeId.val(item);
                    } else {
                        personTypeId.val(personTypeId.val() + "," + item);
                    }
                }                
            });
                        

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        var prevActivityId = $('#activityId').val();

                        if (response.id!=prevActivityId) {
                            var activeFlag = $( "#activeFlag:checked" ).val();
                            SchdActivity.addNode(response.id,$('#activityNameTh').val(),null, activeFlag);
                        } else {
                            
                        }

                        SchdActivity.clear();

                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        update: function(activityId,activitySeqId,refActivityId) {          
               

            $.ajax({
                type: 'POST',
                url: "{{ api('SchdActivity/saveParent') }}",
                data: {
                    activityId: activityId,
                    activitySeqId:activitySeqId,
                    refActivityId:refActivityId
                },
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {                
                        
                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        delete: function(id) {        
            var ref = $('#divActivity').jstree(true),
						sel = ref.get_selected();
						if(!sel.length) { 
                              return false; 
                        }            
                             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + sel[0],
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            SchdActivity.clear();                            
                            
                            ref.delete_node(sel);
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
            
            
        },
        add: function() {
            //console.log($('#divActivity').jstree('get_node', '#', false));

            var seq = $('#divActivity').jstree(true)._model.data['#'].children.length;            
            SchdActivity.clear();
            $('#activitySeqId').val(seq+1);
            $('#activityNameTh').focus();
        },
        addNode: function(activityId,activityName,refActivityId,activeFlag) {                    
            var parent = "";
            if (refActivityId!=null) {
                parent = refActivityId;
            } else {
                parent = '#';
            }

            var activeFlagIcon = "";
            if (activeFlag=="Y") {
                activeFlagIcon = "<i class=\"fa fa-check text-success\"></i>";
            } else {
                activeFlagIcon = "<i class=\"fa fa-times  text-danger\"></i>";
            }

            $('#divActivity').jstree().create_node(parent, {
                "id": activityId,
                "text": activityName + " (" + activeFlagIcon + ")"
            }, "last", function() {
                
            });           
        },
        loadRound : function() {            
            var roundId = $('#roundId').val();            

            $.ajax({
                url: "{{api('SchdRound/get/')}}" + roundId,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        $('#spnRoundInfo').html(response.data.roundNameTh);
                        $('#spnCourseInfo').html($('#courseCd').val());                        
                    } else {
                        Message.error(response);
                    }
                }
            });
        },
        loadActivity : function() {            
            var roundId = $('#roundId').val();            
            var courseCd = $('#courseCd').val();            

            $.ajax({
                url: "{{api('SchdActivity/getByRoundCourse/')}}" + roundId + "/" + courseCd,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {                        
                        $.each(response.data, function (index, item) {                             
                            SchdActivity.addNode(item.activityId,item.activityNameTh,item.refActivityId,item.activeFlag)
                        });                        
                    } else {
                        Message.error(response);
                    }
                }
            });
        },
        edit : function(activityId) {       
              
            SchdActivity.clear();
            $.ajax({
                url: "{{api('SchdActivity/getActivityAndPType/')}}" + activityId,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {           

                        var activity = response.data.activity;
                        var pType = response.data.pType;
                        if (activity) {
                            $('#activityId').val(activity.activityId);
                            $('#activitySeqId').val(activity.activitySeqId);
                            $('#activityNameTh').val(activity.activityNameTh);
                            $('#activityNameEn').val(activity.activityNameEn);
                            $('#refActivityId').val(activity.refActivityId);
                            $('#docId').val(activity.docId).trigger('change');
                            $('#startDate').val(moment(activity.startDate.date).format("DD/MM/YYYY"));
                            $('#endDate').val(moment(activity.endDate.date).format("DD/MM/YYYY"));
                            $('#remark').val(activity.remark);        
                            
                            $('input:radio[name=activityFlag][value='+  activity.activityFlag + ']').attr('checked', true);

                            if (activity.activeFlag=="Y") {
                                $('#activeFlag').prop('checked',true);
                            }   
                        }

                        if (pType) {
                            
                            var personType = [];
                            SchdActivity.personType = [];
                            $.each(pType, function (index, value) {   
                                var v = value.personTypeId.toString();
                                personType.push(v);            
                                SchdActivity.personType.push(value.personTypeId);                    
                            });                             
                                                        
                            $('#personType').multiSelect('select', personType);                            
                            
                        }

                        setTimeout(function(){
                            $('#activityNameTh').select();
                        },200);
                        
                    } else {
                        Message.error(response);
                    }
                }
            });
        }
    }
    
</script>
@endsection