<form id="frmSave" action="{{ api('SchdActivity/save')}}" method="post">
    <input type="hidden" name="roundId" id="roundId" value="{{ $roundId }}">
    <input type="hidden" name="courseCd" id="courseCd" value="{{ $courseCd }}">
    <input type="hidden" name="activityId" id="activityId">
    <input type="hidden" name="refActivityId" id="refActivityId">
    <input type="hidden" name="personTypeId" id="personTypeId">
    

    <div class="card">
        <div class="card-body">    
            <div class="form-group row">
                <label for="activitySeqId" class="col-md-2 ">ลำดับที่</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="activitySeqId" name="activitySeqId" autocomplete="off" maxlength="3" required readOnly>
                </div>                
            </div>        
            <div class="form-group row">
                <label for="activityNameTh" class="col-md-2 ">ชื่อกิจกรรม (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="activityNameTh" name="activityNameTh" required autocomplete="off" maxlength="500" required>
                </div>                
            </div>
            <div class="form-group row">
                <label for="roundNameEn" class="col-md-2">ชื่อกิจกรรม (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="activityNameEn" name="activityNameEn" autocomplete="off" maxlength="500">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-2">ช่วงวันที่กิจกรรม</label>
                <div class="col-md-4">
                    <div class="input-group-append">
                        <input class="form-control" type="text" id="startDate" name="startDate" autocomplete="off" maxlength="10">&nbsp;-&nbsp;
                        <input class="form-control" type="text" id="endDate" name="endDate" autocomplete="off" maxlength="10">
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <label class="col-md-2">เป็นกิจกรรมแบบ</label>
                <div class="col-md-10">
                    <div class="form-group d-inline">
                        <div class="radio radio-primary d-inline">
                            <input type="radio" name="activityFlag" id="rdoActivityFlagActivity" value="A" checked>
                            <label for="rdoActivityFlagActivity" class="cr">กิจกรรม</label>
                        </div>
                    </div>
                    <div class="form-group d-inline">
                        <div class="radio radio-primary d-inline">
                            <input type="radio" name="activityFlag" id="rdoActivityFlagSeminar" value="S">
                            <label for="rdoActivityFlagSeminar" class="cr">สัมมนา</label>
                        </div>
                    </div>
                    <div class="form-group d-inline">
                        <div class="radio radio-primary d-inline">
                            <input type="radio" name="activityFlag" id="rdoActivityFlagDoc" value="D">
                            <label for="rdoActivityFlagDoc" class="cr">เอกสาร</label>
                        </div>
                    </div>
                </div>                                
            </div>

            <div class="form-group row">
                <label for="roundNameEn" class="col-md-2">เอกสารที่ใช้ในกิจกรรม</label>
                <div class="col-md-10">
                    <select name="docId" id="docId" class="form-control">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="remark" class="col-md-2">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" class="form-control" maxlength="400"></textarea>
                </div>                    
            </div>
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>        
                        
            <div class="form-group row">
                <label for="remark" class="col-md-2">ประเภทบุคลากร</label>
                <div class="col-md-10">
                    <select id="personType" name="personType">
                    </select>   
                </div>                 
            </div> 
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>   