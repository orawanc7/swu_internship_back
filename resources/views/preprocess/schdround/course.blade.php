<div class="form-group row">
    <div class="col-md-2">
        วิชา
    </div>
    <div class="col-md-5">
        <div class="input-group">
            <input type="text" name="courseCd" id="courseCd" class="form-control text-uppercase">
            <button class="btn btn-sm btn-info" id="btnAddCourse" type="button"><i class="fa fa-plus"></i>เพิ่มวิชา</button>
        </div>        
    </div>
    <div class="col-md-5">
        <table id="tableCourse" class="display table table-striped table-hover dt-responsive nowrap" style="width:100%">
            <thead>
                <tr>
                    <th style="text-align:left;width:90%;">รหัสวิชา</th>                
                    <th style="text-align:center;width:20%"></th>                    
                </tr>
            </thead>
            <tbody>                                             
            </tbody>                        
        </table>
    </div>
</div>