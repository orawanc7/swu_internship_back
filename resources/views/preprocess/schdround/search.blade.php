<div class="dt-responsive table-responsive">
    <table id="table" class="display table table-striped table-hover dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th style="text-align:center;width:10%;">รหัสรอบ</th>
                <th style="text-align:left;width:10%;">ภาค/ปีการศึกษา</th>
                <th style="text-align:left;width:30%;">ชื่อรอบการฝึกปฏิบัติการสอน (ไทย)</th>   
                <th style="text-align:left;width:20%;">ช่วงวันที่ฝึกปฏิบัติการสอน</th>                                                            
                <th style="text-align:center;width:10%">สถานะ</th>    
                <th style="text-align:center;width:20%"></th>                    
            </tr>
        </thead>
        <tbody>                             
        </tbody>                        
    </table>
</div>