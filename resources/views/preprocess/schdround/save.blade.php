<form id="frmSave" action="{{ api('SchdRound/save')}}" method="post">
    <input type="hidden" name="roundId" id="roundId">
    <div class="card">
        <div class="card-body">                        
            <div class="form-group row">
                <label for="roundNameTh" class="col-md-2 ">ชื่อรอบการฝึกปฏิบัติการสอน (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="roundNameTh" name="roundNameTh" required autocomplete="off" maxlength="80" required>
                </div>                
            </div>
            <div class="form-group row">
                <label for="roundNameEn" class="col-md-2">ชื่อรอบการฝึกปฏิบัติการสอน (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="roundNameEn" name="roundNameEn" autocomplete="off" maxlength="80">
                </div>
            </div>
            <div class="form-group row">
                <label for="remark" class="col-md-2">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" class="form-control" maxlength="400"></textarea>
                </div>                    
            </div>
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>

            @include('preprocess.schdround.course')

        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-sm btn-info" type="button" id="btnSchool"><i class="fa fa-school"></i> โรงเรียน</button>
                </div>
                <div class="col-md-6 text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
                    <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>            
            </div>            
        </div>
    </div>     
</form>   