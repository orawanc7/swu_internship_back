@extends('layout.app')

@section('style')
@include('plugin.datetime.css')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">รอบการฝึกปฏิบัติการสอน</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลก่อนการฝึกสอน</a></li>
    <li class="breadcrumb-item"><a href="#!">รอบการฝึกปฏิบัติการสอน</a></li>
</ul>
@endsection

@section('content')
<ul class="nav nav-tabs" id="tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="search" aria-selected="true"><i class="fa fa-search"></i> ค้นหา</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="save-tab" data-toggle="tab" href="#save" role="tab" aria-controls="save" aria-selected="false"><i class="fa fa-save"></i> บันทึก</a>
    </li>    
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="search-tab">
        @include('preprocess.schdround.search')
    </div>
    <div class="tab-pane fade" id="save" role="tabpanel" aria-labelledby="save-tab">
        @include('preprocess.schdround.save')
    </div>    
</div>
@endsection


@section('script') 
@include('plugin.datetime.js')
@include('plugin.datatable.js')
<script>   
    var table;
    var tableCourse;
    var frmObj;
    $(document).ready(function () {    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
                                            
        $('#btnCancel').click(function(){           
            SchdRound.clear();
        });

        $('#btnSchool').click(function(){
            location.href = "{{ route('schdschool') }}/" + $('#roundId').val() ;
        });

        $("#year").inputmask({
            mask: "9999",
            greedy: true 
        });


        $('#startDate').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            time:false
        });
        $('#endDate').bootstrapMaterialDatePicker({
            format: 'DD/MM/YYYY',
            time:false
        });

        SchdRound.initMain();
        SchdRound.initMainTable();
        SchdRound.initCourse();
    });

    var SchdRound = {
        initMain() {
            frmObj = $('#frmSave').validate();        
            $('#frmSave').submit(function (e) {                 
                e.preventDefault();                                        
                var frm = $(this);
                if (frmObj.valid()) {
                    MessageConfirm.save(function() {
                        SchdRound.save(frm);
                    },function() {

                    });
                }
            });
        },
        initMainTable() {
            table = $('#table').DataTable( {            
                ajax: "{{api('SchdRound/getAll')}}",
                type:'GET',
                dataType:'json',
                columns:[
                    {'data':'roundId'},                
                    {'data':'year', render: function(data,type,row) {
                        return row.semCd + "/"  + row.year;
                    }},
                    {'data':'roundNameTh'},
                    {'data':'startDate', render: function(data,type,row) {
                        var startDate = moment(row.startDate.date).format("DD/MM/YYYY");
                        var endDate = moment(row.endDate.date).format("DD/MM/YYYY");
                        return startDate + " - "  + endDate;                    
                    }},
                    {'data':'activeFlag',render: function(data) {
                        if (data=="Y") {
                            return "<i class=\"fa fa-check text-success\"></i>";
                        } else {
                            return "";
                        }
                    }},                
                    {'data':'roundId' , render: function(data){
                        return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:SchdRound.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                        "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchdRound.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                    }}
                ],      
                columnDefs: [
                    {
                        "className": "text-center", "targets": [0,2,3,4],                                                        
                    },
                    {
                        "className": "text-right", "targets": [5],
                    },
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 5 }            
                ],                
                buttons: [
                    {
                        text: "<i class=\"fa fa-plus\"></i>",
                        className: "btn btn-info",
                        action: function (e, dt, node, config) {
                            SchdRound.add();
                        }
                    },
                    {
                        extend: 'print',
                        title: "รอบการฝึกปฏิบัติการสอน",
                        text: "<i class=\"fa fa-print\"></i> พิมพ์",
                        className: "btn btn-primary",
                        exportOptions: {
                            modifier: {
                                order: 'index', // 'current', 'applied',                                
                                page: 'all', // 'all', 'current'
                                search: 'none' // 'none', 'applied', 'removed'
                            }
                        }
                    },
                    {
                        extend: 'excel',
                        title: "รอบการฝึกปฏิบัติการสอน",
                        text: "<i class=\"fa fa-file-excel\"></i> Excel",
                        className: "btn btn-success",
                        exportOptions: {
                            modifier: {
                                order: 'index', // 'current', 'applied',                                
                                page: 'all', // 'all', 'current'
                                search: 'none' // 'none', 'applied', 'removed'
                            }
                        }
                    },
                ],     
                "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-6'i><'col-md-6'p>>",
                'processing': true         
            });
        },        
        initCourse() {
            $('#tableCourse tbody').on('click','button.delete-course',function(){
                $(this).closest('tr').remove();
            });

            $('#tableCourse tbody').on('click','button.activity-course',function(){
                var id = $(this).closest('tr').attr('data-id');
                location.href = "{{ route('schdactivity') }}/" + $('#roundId').val() + "/"  + id;
            });

            $('#btnAddCourse').click(function(){
                var courseCd = $('#courseCd');
                SchdRound.addCourse( courseCd.val().toUpperCase() );
                courseCd.val("");
                courseCd.focus();
            });
        },
        addCourse: function(courseCd) {            
            $('#tableCourse tbody').append(
                   "<tr data-id=\"" + courseCd + "\">" +
                    "<td>" +
                        "<input type='hidden' name='courseCd[]' value='" + courseCd + "'>" + courseCd +
                    "</td>" +
                    "<td class=\"text-right\">" +
                        "<button type=\"button\" class=\"btn btn-sm btn-danger delete-course\"><i class='fa fa-trash'></i>ลบ</button>" +
                        "<button type=\"button\" class=\"btn btn-sm btn-primary activity-course\">กิจกรรม</button>" +
                    "</td>" +
                    "</tr>"
            );
        },
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#roundId').val("");
            frmObj.resetForm();
            $('#tableCourse tbody').empty();
            $('#semCd').focus();            
        },
        add: function() {
            SchdRound.clear();
            $('a[href="#save"]').tab('show');            
            setTimeout(function() {
                $('#roundId').select();
            },300);
        },
        edit: function(id) {            
            $.ajax({
                url: "{{api('SchdRound/get/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        SchdRound.clear();

                        var data = response.data;
                        
                        $('#roundId').val(data.roundId);
                        $('#year').val(data.year);
                        $('#semCd').val(data.semCd);
                        $('#roundNameTh').val(data.roundNameTh);
                        $('#roundNameEn').val(data.roundNameEn);
                        $('#startDate').val(moment(data.startDate.date).format("DD/MM/YYYY"));
                        $('#endDate').val(moment(data.endDate.date).format("DD/MM/YYYY"));
                        $('#remark').val(data.remark);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }                        

                        $('a[href="#save"]').tab('show');

                        SchdRound.getCourse(data.roundId);

                        setTimeout(function() {
                            $('#semCd').select();
                        },300);                        
                    } else {
                        Message.error(response);
                    }
                }
            });
        },
        delete: function(id) {             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            SchdRound.clear();
                            table.ajax.reload();

                            $('#semCd').focus();
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });                        
        },
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        $('roundId').val(response.id);

                        //SchdRound.clear();
                        table.ajax.reload();                        
                        $('#semCd').focus();

                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        getCourse : function(roundId) {            
            $('#tableCourse tbody').empty();

            $.ajax({
                url: "{{api('SchdRoundCourse/getByRoundId/')}}" + roundId,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        console.log(response.data);
                        $.each(response.data, function (index, item) {                             
                            SchdRound.addCourse(item.courseCd);
                        });                                                                                
                    } else {
                        Message.error(response);
                    }
                }
            });
        }
    };
    
</script>
@endsection
