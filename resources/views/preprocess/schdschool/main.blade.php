@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">โรงเรียน</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลก่อนการฝึกสอน</a></li>
    <li class="breadcrumb-item"><a href="{{ route('schdround')}}">รอบการฝึกปฏิบัติการสอน</a></li>
    <li class="breadcrumb-item"><a href="#!">โรงเรียน</a></li>
</ul>
@endsection

@section('content')
<div class="card">
    <div class="card-header bg-info">
        <div class="row">
            <div class="col-sm-6">
                <h5 class="text-white"><i class="fa fa-book"></i>  <span id="spnRoundInfo"></h5>

            </div>            
        </div>        
    </div>
    <div class="card-body">        
        @include('preprocess.schdschool.save')                 
        @include('preprocess.schdschool.search')    
    </div>
</div>

@endsection

@section('script')
@include('plugin.datatable.js')
<script>
    var table;
    var frmObj;    
    $(document).ready(function () {      
        $('#btnCancel').click(function(e){
            e.preventDefault();
            SchdSchool.clear();
        });                
        
        $('#schoolId').select2({
            width:'100%'
        });

        SchdSchool.loadRound();  
        SchdSchool.loadSchool();  
        SchdSchool.initMain();     
        SchdSchool.initMainTable();                         
    });
   
    var SchdSchool = {  
        initMain() {
            frmObj = $('#frmSave').validate();        
            $('#frmSave').submit(function (e) {                 
                e.preventDefault();                                        
                var frm = $(this);
                if (frmObj.valid()) {
                    MessageConfirm.save(function() {
                        SchdSchool.save(frm);
                    },function() {

                    });
                }
            });
        },      
        initMainTable() {
            table = $('#table').DataTable( {            
                ajax: "{{api('SchdSchool/getByRoundId')}}/{{ $roundId }}",
                type:'GET',
                dataType:'json',
                columns:[
                    {'data':'schoolId'},                
                    {'data':'schoolNameTh'},
                    {'data':'provinceNameTh'},
                    {'data':'schoolHdrId' , render: function(data){
                        return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:SchdSchool.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:SchdSchool.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                    }}
                ],      
                columnDefs: [
                    {
                        "className": "text-center", "targets": [0],                                                        
                    },
                    {
                        "className": "text-right", "targets": [3],
                    },
                    { responsivePriority: 1, targets: 0 },
                    { responsivePriority: 2, targets: 3 }            
                ],                
                buttons: [
                    {
                        text: "<i class=\"fa fa-plus\"></i>",
                        className: "btn btn-info",
                        action: function (e, dt, node, config) {
                            SchdSchool.add();
                        }
                    },
                    {
                        extend: 'print',
                        title: "โรงเรียน",
                        text: "<i class=\"fa fa-print\"></i> พิมพ์",
                        className: "btn btn-primary",
                        exportOptions: {
                            modifier: {
                                order: 'index', // 'current', 'applied',                                
                                page: 'all', // 'all', 'current'
                                search: 'none' // 'none', 'applied', 'removed'
                            }
                        }
                    },
                    {
                        extend: 'excel',
                        title: "โรงเรียน",
                        text: "<i class=\"fa fa-file-excel\"></i> Excel",
                        className: "btn btn-success",
                        exportOptions: {
                            modifier: {
                                order: 'index', // 'current', 'applied',                                
                                page: 'all', // 'all', 'current'
                                search: 'none' // 'none', 'applied', 'removed'
                            }
                        }
                    },
                ],     
                "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                    "<'row'<'col-md-12'tr>>" +
                    "<'row'<'col-md-6'i><'col-md-6'p>>",
                'processing': true                
            });
        },   
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#schoolHdrId').val("");                                
            $('#schoolId').val("").trigger("change");    
            frmObj.resetForm();            
        },
        add: function() {
            SchdSchool.clear();     
            $('#schoolId').focus();               
        },
        save: function(frm) {                  
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        
                        SchdSchool.clear();                        
                        table.ajax.reload();

                        $('#schoolId').focus();

                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },        
        edit: function(id) {            
            $.ajax({
                url: "{{api('SchdSchool/get/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {                        
                        SchdSchool.clear();

                        var data = response.data;
                        $('#schoolHdrId').val(data.schoolHdrId);
                        $('#schoolId').val(data.schoolId).trigger('change');
                        $('#remark').val(data.remark);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }                        
                        $('a[href="#save"]').tab('show');
                        setTimeout(function() {
                            $('#schoolId').select();
                        },300);     
                    }
                }
            });
        },
        delete: function(id) {                           
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            SchdSchool.clear();                            
                                                        
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
                        
        },
        loadSchool () {
            $('#schoolId').empty();            

            $.getJSON("{{ api('School' )}}", function(data){             
                $("#schoolId").append("<option value=''></option>");    
                $.each(data.data, function(index,item) {                                    
                    $("#schoolId").append("<option value=" + item.schoolId + ">" + item.schoolNameTh + " (" + item.provinceNameTh +")" + "</option>"); 
                });
            });
        },
        loadRound : function() {            
            var roundId = $('#roundId').val();            

            $.ajax({
                url: "{{api('SchdRound/get/')}}" + roundId,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        $('#spnRoundInfo').html(response.data.roundNameTh);                                          
                    } else {
                        Message.error(response);
                    }
                }
            });
        }             
    }
    
</script>
@endsection