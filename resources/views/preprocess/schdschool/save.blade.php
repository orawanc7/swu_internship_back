<form id="frmSave" action="{{ api('SchdSchool/save')}}" method="post">   
<input type="hidden" name="roundId" id="roundId" value="{{ $roundId }}">    
<input type="hidden" name="schoolHdrId" id="schoolHdrId">    
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <div class="col-md-2">
                    โรงเรียน
                </div>
                <div class="col-md-10">            
                    <select name="schoolId" id="schoolId" class="form-control" required></select>                            
                </div>        
            </div>
            <div class="form-group row">
                <label for="remark" class="col-md-2">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" class="form-control" maxlength="400"></textarea>
                </div>                    
            </div>         
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>