<div class="dt-responsive table-responsive">
    <table id="table" class="display table table-striped table-hover dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th style="text-align:center;width:20%;">รหัสโรงเรียน</th>
                <th style="text-align:left;width:40%;">ชื่อโรงเรียน</th>
                <th style="text-align:left;width:30%;">จังหวัด</th>   
                <th style="text-align:center;width:10%"></th>                    
            </tr>
        </thead>
        <tbody>                             
        </tbody>                        
    </table>
</div>