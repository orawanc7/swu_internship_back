@extends('layout.public')

@section('content')
<!-- [ auth-signin ] start -->
<div class="auth-wrapper">
	<div class="auth-content container">
		<div class="card">
			<div class="row align-items-center">
				<div class="col-md-6">
					<div class="card-body">
                        <img src="@relative('assets/images/logo.png')" alt="" class="img-fluid mb-4">
                        <form action="{{ route('login') }}" method="post" id="frmLogin">
                            <h4 class="mb-3 f-w-400">เข้าสู่ระบบด้วย Buasri ID</h4>
                            <div class="form-group input-group mb-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-mail"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Buasri ID" autofocus id="buasriId" name="buasriId" required>                                
                            </div>
                            <div class="form-group input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="feather icon-lock"></i></span>
                                </div>
                                <input type="password" class="form-control" placeholder="Password" id="busriPassword" name="busriPassword" required>
                            </div>
                            <button class="btn btn-primary mb-4" type="submit">เข้าสู่ระบบ</button>			
                        </form>			
					</div>
				</div>
				<div class="col-md-6 d-none d-md-block">
					<img src="@relative('assets/images/auth-bg.jpg')" alt="" class="img-fluid" width="50px;">
				</div>
			</div>
		</div>
	</div>
</div>
<!-- [ auth-signin ] end -->   
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('#frmLogin').validate();
    });
</script>
@endsection
