@extends('admin.layout')

@section('content')

<div class="row">
    <div class="col-sm-12 p-0">
        <div class="main-header">            
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item "><i class="icofont icofont-home"></i>
                    <a href="">หน้าหลัก</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">ข้อมูลเผยแพร่</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin/docgroup') }}">กลุ่มเอกสารเผยแพร่</a>
                </li>
                <li class="breadcrumb-item active">เอกสารเผยแพร่</li>                
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="frmSave" action="{{ route('api/Download/save')}}" method="post">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            กลุ่มเอกสารเผยแพร่ : {{ $docGroup['docGroupNameTh']  }}
                        </div>                        
                    </div>                    
                </div>
                <div class="card-block">
                    <div class="form-group row">
                        <label for="docTitleNameTh" class="col-md-2 col-form-label mandatory">หัวข้อภาษาไทย</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="docTitleNameTh" name="docTitleNameTh" required autocomplete="off" maxlength="200">
                        </div>
                    </div>                        
                    <div class="form-group row">
                        <label for="docTitleNameEn" class="col-md-2 col-form-label">หัวข้อภาษาอังกฤษ</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="docTitleNameEn" name="docTitleNameEn" autocomplete="off" maxlength="200">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="remarkTh" class="col-md-2 col-form-label">หมายเหตุไทย</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="remarkTh" name="remarkTh" autocomplete="off" maxlength="200">
                        </div>                    
                    </div>
                    <div class="form-group row">
                        <label for="remarkEn" class="col-md-2 col-form-label">หมายเหตุภาษาอังกฤษ</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" id="remarkEn" name="remarkEn" autocomplete="off" maxlength="200">
                        </div>                    
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 offset-md-2 checkbox-color checkbox-primary">
                            <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                            <label for="activeFlag" class="text-primary">
                                    สถานะ
                            </label>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;<button class="btn btn-sm btn-default" id="btnCancel"><i class="fa fa-undo"></i> ยกเลิก</button>
                </div>
            </div>     
        </form>   
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">        
            <div class="card-block">                
                <table id="table" class="table dt-responsive table-striped nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="text-align:center;width:15%;">รหัสกลุ่ม</th>
                            <th style="text-align:left;width:30%;">กลุ่มเอกสารไทย</th>
                            <th style="text-align:left;width:20%;">กลุ่มเอกสารอังกฤษ</th>                                
                            <th style="text-align:center;width:10%">สถานะ</th>    
                            <th style="text-align:center;width:10%">เอกสาร</th>    
                            <td style="text-align:center;width:15%">การจัดการ</td>                    
                        </tr>
                    </thead>
                    <tbody>                             
                    </tbody>                        
                </table>                
            </div>
        </div>
    </div>
</div>    
@endsection


@section('script') 
<script src="@asset('js/default.datatables.js')"></script>
<script>   
    var table;
    $(document).ready(function () {    
                            
        table = $('#table').DataTable( {
            ajax: "{{route('api/DocGroup')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'docGroupId'},
                {'data':'docGroupNameTh'},
                {'data':'docGroupNameEn'},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check\"></i>";
                    } else {
                        return "";
                    }
                }},                                
                {'data':'docGroupId' ,sortable:false , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"{{ route('admin/download') }}/" + data + "\"><i class=\"fas fa-file\"></i> เอกสาร</a>";
                }},
                {'data':'docGroupId' , sortable:false, render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:DocGroup.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:DocGroup.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3,4,5],
                },
            ],         
            processing: true
        });       
             
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            DocGroup.save($(this));

        });

        $('#btnCancel').click(function(){
            DocGroup.clear();
        });
    });

    var DocGroup = {
        clear: function () {            
            $('#frmSave')[0].reset();
        },
        edit: function(id) {
            $.ajax({
                url: "{{route('api/DocGroup/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {
                        DocGroup.clear();

                        var data = response.data;
                        $('#docGroupId').val(data.docGroupId);
                        $('#docGroupNameTh').val(data.docGroupNameTh);
                        $('#docGroupNameEn').val(data.docGroupNameEn);
                        $('#remark').val(data.remark);                        
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('#docGroupNameTh').focus();
                    }
                }
            });
        },
        delete: function(id) {                        
            $.ajax({
                type: 'DELETE',
                url: $('#frmSave').attr('action') + '/' + id,
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        DocGroup.clear();
                        table.ajax.reload();

                        $('#docGroupId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });
        },
        save: function(frm) {

            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        DocGroup.clear();
                        table.ajax.reload();

                        $('#docGroupId').focus();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
