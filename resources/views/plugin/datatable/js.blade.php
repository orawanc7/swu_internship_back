<script src="@relative('assets/plugins/data-tables/js/datatables.min.js')"></script>
<script src="@relative('assets/js/pages/data-ajax-custom.js')"></script>
<script src="@relative('assets/plugins/data-tables/js/buttons.colVis.min.js')"></script>
<script src="@relative('assets/js/pages/data-buttons-custom.js')"></script>
<script src="@relative('assets/config/default.datatables.js')"></script>