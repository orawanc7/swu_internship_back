@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">ประเภทเอกสาร</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลเอกสารและแบบฟอร์ม</a></li>
    <li class="breadcrumb-item"><a href="#!">ประเภทเอกสาร</a></li>
</ul>
@endsection

@section('content')
<ul class="nav nav-tabs" id="tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="search" aria-selected="true"><i class="fa fa-search"></i> ค้นหา</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="save-tab" data-toggle="tab" href="#save" role="tab" aria-controls="save" aria-selected="false"><i class="fa fa-save"></i> บันทึก</a>
    </li>    
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="search-tab">
        @include('document.doctype.search')
    </div>
    <div class="tab-pane fade" id="save" role="tabpanel" aria-labelledby="save-tab">
        @include('document.doctype.save')
    </div>    
</div>
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>   
    var table;
    var frmObj;
    $(document).ready(function () {    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
                                    
        table = $('#table').DataTable( {            
            ajax: "{{api('DocType')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'docTypeId'},
                {'data':'docTypeTh'},
                {'data':'docScreenFlag',render: function(data) {
                    return (data=="S")?"Screen":((data=="F")?"Form":"");
                }},
                {'data':'activeFlag','width':'10%',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check text-success\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'docTypeId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:DocType.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:DocType.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,2,3],                                                        
                },
                {
                    "className": "text-right", "targets": [4],
                }    
            ],                
            buttons: [
                {
                    text: "<i class=\"fa fa-plus\"></i>",
                    className: "btn btn-info",
                    action: function (e, dt, node, config) {
                        DocType.add();
                    }
                },
                {
                    extend: 'print',
                    title: "ประเภทเอกสาร",
                    text: "<i class=\"fa fa-print\"></i> พิมพ์",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "ประเภทเอกสาร",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],     
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            'processing': true,
            'responsive': true,                
        });       
        frmObj = $('#frmSave').validate();
        
        $('#frmSave').submit(function (e) { 
            
            e.preventDefault();
                                    
            var frm = $(this);

            if (frmObj.valid()) {
                MessageConfirm.save(function() {
                    DocType.save(frm);
                },function() {

                });
            }

        });

        $('#btnCancel').click(function(){           
            DocType.clear();
        });

        $("#docTypeId").inputmask({
            mask: "9[9]",
            greedy: false 
        });
    });

    var DocType = {
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#docTypeId').val("");
            frmObj.resetForm();
            $('#docTypeId').focus();            
        },
        add: function() {
            DocType.clear();
            $('a[href="#save"]').tab('show');
            setTimeout(function() {
                $('#docTypeId').select();
            },300);
        },
        edit: function(id) {            
            $.ajax({
                url: "{{api('DocType/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        DocType.clear();

                        var data = response.data;
                        $('#docTypeId').val(data.docTypeId);
                        $('#docTypeTh').val(data.docTypeTh);
                        $('#docTypeEn').val(data.docTypeEn);
                        $('#remark').val(data.remark);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }
                        $('input:radio[name=docScreenFlag][value='+  data.docScreenFlag + ']').attr('checked', true);

                        $('a[href="#save"]').tab('show');
                        setTimeout(function() {
                            $('#docTypeTh').select();
                        },300);                        
                    } else {
                        Message.error(response);
                    }
                }
            });
        },
        delete: function(id) {             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            DocType.clear();
                            table.ajax.reload();

                            $('#docTypeId').focus();
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
            
            
        },
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        //DocType.clear();
                        table.ajax.reload();                        
                        $('#docTypeTh').focus();

                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });

        }
    };
    
</script>
@endsection
