<form id="frmSave" action="{{ api('DocType/save')}}" method="post">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="docTypeId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="docTypeId" name="docTypeId" required autofocus autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <label for="docTypeTh" class="col-md-2 col-form-label mandatory">ประเภทเอกสารไทย</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docTypeTh" name="docTypeTh" required autocomplete="off">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="docTypeTh" class="col-md-2 col-form-label mandatory">ประเภทเอกสารอังกฤษ</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docTypeEn" name="docTypeEn" autocomplete="off">
                </div>                        
            </div> 

            <div class="form-group row">
                <label class="col-md-2">ลักษณะการแสดง</label>
                <div class="col-md-10">
                    <div class="form-group d-inline">
                        <div class="radio radio-primary d-inline">
                            <input type="radio" name="docScreenFlag" id="rdoDocScreenFlagScreen" value="S" checked>
                            <label for="rdoDocScreenFlagScreen" class="cr">Screen</label>
                        </div>
                    </div>
                    <div class="form-group d-inline">
                        <div class="radio radio-primary d-inline">
                            <input type="radio" name="docScreenFlag" id="rdoDocScreenFlagForm" value="F">
                            <label for="rdoDocScreenFlagForm" class="cr">Form</label>
                        </div>
                    </div>
                </div>                                
            </div>

            <div class="form-group row">
                <label for="remark" class="col-md-2 col-form-label">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" rows="5" class="form-control"></textarea>
                </div>                        
            </div>               
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>