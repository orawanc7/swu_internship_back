<form id="frmSave" action="{{ api('Doc/save')}}" method="post">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="docId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docId" name="docId" required autofocus autocomplete="off" maxlength="3">
                </div>                
            </div>
            <div class="form-group row">
                <label for="docTypeId" class="col-md-2 col-form-label mandatory">ประเภทเอกสาร</label>
                <div class="col-md-10">
                    <select name="docTypeId" id="docTypeId" class="form-control select2" required></select>
                </div>
            </div>
            <div class="form-group row">
                <label for="docNameTh" class="col-md-2 col-form-label mandatory">ชื่อเอกสารไทย</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docNameTh" name="docNameTh" required autocomplete="off" maxlength="200">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="docNameTh" class="col-md-2 col-form-label mandatory">ชื่อเอกสารอังกฤษ</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="docNameEn" name="docNameEn" autocomplete="off" maxlength="200">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="docCdTh" class="col-md-2 col-form-label mandatory">รหัสเอกสารไทย</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="docCdTh" name="docCdTh" autocomplete="off" maxlength="10">
                </div>         
                <label for="docCdEn" class="col-md-2 col-form-label mandatory">รหัสเอกสารไทย</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="docCdEn" name="docCdEn" autocomplete="off" maxlength="10">
                </div>                     
            </div> 
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="docFinalFlag"  name="docFinalFlag" type="checkbox" value="Y">
                    <label for="docFinalFlag" class="text-primary">
                        เอกสารส่งงานปลายภาค
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label for="docNameTh" class="col-md-2 col-form-label">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" rows="5" class="form-control"></textarea>
                </div>                        
            </div>               
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>