<form id="frmSave" action="{{ api('Frm/save')}}" method="post">
    <input type="hidden" name="frmHdrId" id="frmHdrId">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="frmNameTh" class="col-md-2 col-form-label mandatory">ชื่อแบบฟอร์มไทย</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="frmNameTh" name="frmNameTh" required autocomplete="off">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="frmNameTh" class="col-md-2 col-form-label mandatory">ชื่อแบบฟอร์มอังกฤษ</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="frmNameEn" name="frmNameEn" autocomplete="off">
                </div>                        
            </div> 
            
            <div class="form-group row">
                <label for="docId" class="col-md-2 col-form-label mandatory">เอกสาร</label>
                <div class="col-md-10">
                    <select name="docId" id="docId" class="form-control select2" required></select>
                </div>
            </div>

            <div class="form-group row">
                <label for="remark" class="col-md-2 col-form-label">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" rows="5" class="form-control"></textarea>
                </div>                        
            </div>               
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>