@extends('layout.app')

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">แบบฟอร์ม</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลเอกสารและแบบฟอร์ม</a></li>
    <li class="breadcrumb-item"><a href="{{ route('form') }}">แบบฟอร์ม</a></li>    
    <li class="breadcrumb-item"><a href="#!">ออกแบบ</a></li>    
</ul>
@endsection

@section('content')

<div class="row">
    <div class="col-md-6">
        <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        <button class="btn btn-sm btn-info" id="btnClear"><i class="fa fa-eraser"></i> ล้าง</button>
        <button class="btn btn-sm btn-success" id="btnSave"><i class="fa fa-save"></i> บันทึก</button>
    </div>
    <div class="col-md-6 text-right">
        <button class="btn btn-sm btn-info" id="btnPreview"><i class="fa fa-print"></i> ตัวอย่าง</button>
    </div>
</div>

<textarea id="form-data" >
</textarea>

<form id="frmSave" action="{{ api('Frm/saveTemplate')}}" method="post">
    <input type="hidden" id="frmHdrId" name="frmHdrId" value="{{ $frmHdrId }}">
    <input type="hidden" id="frmTemplate" name="frmTemplate">
</form>    

@endsection

@section('script') 
@include('plugin.jqueryui.js')
@include('plugin.formbuilder.js')

<script>
    var fbEditor;
    var formBuilder;
    var options = {        
        showActionButtons: false, 
        disableFields: [
            'autocomplete',
            'button',
            'hidden',
            'file'
        ],
        controlOrder: [
            'header',
            'paragraph',            
            'text',
            'number',
            'date',
            'time',            
            'select',            
            'radio-group',            
            'checkbox-group',            
            'textarea'
        ]
    };
    
    $(document).ready(function () {
        fbEditor = document.getElementById("form-data");
        formBuilder = $(fbEditor).formBuilder(options);        

        document.getElementById("btnSave").addEventListener("click", function () {            
            var result = formBuilder.actions.save();            
            $('#frmTemplate').val(result);
            
            MessageConfirm.save(function() {
                FormDesign.save($('#frmSave'));
            },function() {

            });                            
        });

        $('#btnCancel').click(function(e){
            e.preventDefault();
            location.href = "{{ route('form') }}";
        });          
        
        $('#btnClear').click(function (e) { 
            e.preventDefault();
            
            formBuilder.actions.clearFields();
        });

        $('#btnPreview').click(function (e) { 
            e.preventDefault();
        });
        
        FormDesign.loadTemplate("{{ $frmHdrId }}");
    });

    var FormDesign = {
        loadTemplate: function(id) {
            $.ajax({
                url: "{{api('Frm/getTemplate/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                              
                    if (response.data) {
                        
                        var data = response.data;
                                                                                                                                                
                        formBuilder.actions.setData(data.frmTemplate);

                    } else {
                        Message.error(response);
                    }
                }
            });
        },
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {                        
                        MessageNotify.saveSucess();
                    } else {
                        alert(response.message);
                    }
                }
            });            
        },
    };
</script>
@endsection