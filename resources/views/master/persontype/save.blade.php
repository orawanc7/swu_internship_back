<form id="frmSave" action="{{ api('PersonType/save')}}" method="post">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="personTypeId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="personTypeId" name="personTypeId" required autofocus autocomplete="off" maxlength="3">
                </div>                
            </div>
            <div class="form-group row">
                <label for="personGroupId" class="col-md-2 col-form-label mandatory">กลุ่มบุคลากร</label>
                <div class="col-md-10">
                    <select name="personGroupId" id="personGroupId" class="form-control select2"></select>
                </div>
            </div>
            <div class="form-group row">
                <label for="personTypeNameTh" class="col-md-2 col-form-label mandatory">ชื่อประเภทบุคคลากร (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="personTypeNameTh" name="personTypeNameTh" required autocomplete="off" maxlength="200">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="personTypeNameTh" class="col-md-2 col-form-label mandatory">ชื่อประเภทบุคคลากร (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="personTypeNameEn" name="personTypeNameEn" autocomplete="off" maxlength="200">
                </div>                        
            </div>             
            <div class="form-group row">
                <label for="personTypeNameTh" class="col-md-2 col-form-label">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" rows="5" class="form-control"></textarea>
                </div>                        
            </div>               
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>