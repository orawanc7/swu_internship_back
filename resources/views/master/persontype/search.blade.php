<div class="dt-responsive table-responsive">
    <table id="table" class="display table table-striped table-hover dt-responsive nowrap" style="width:100%">
        <thead>
            <tr>
                <th style="text-align:center;width:20%;">รหัส</th>
                <th style="text-align:left;width:30%;">ประเภทบุคคลากร</th>
                <th style="text-align:left;width:20%;">กลุ่มบุคลากร</th>                                
                <th style="text-align:center;width:10%">สถานะ</th>                        
                <th style="text-align:center;width:20%"></th> 
            </tr>
        </thead>
        <tbody>                             
        </tbody>                        
    </table>
</div>