@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">ประเภทบุคลากร</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลพื้นฐานระบบนิเทศ</a></li>
    <li class="breadcrumb-item"><a href="#!">ประเภทบุคลากร</a></li>
</ul>
@endsection

@section('content')
<ul class="nav nav-tabs" id="tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="search" aria-selected="true"><i class="fa fa-search"></i> ค้นหา</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="save-tab" data-toggle="tab" href="#save" role="tab" aria-controls="save" aria-selected="false"><i class="fa fa-save"></i> บันทึก</a>
    </li>    
</ul>
<div class="tab-content" id="tabContent">
    <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="search-tab">
        @include('master.persontype.search')
    </div>
    <div class="tab-pane fade" id="save" role="tabpanel" aria-labelledby="save-tab">
        @include('master.persontype.save')
    </div>    
</div>
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>   
    var table;
    var frmObj;
    $(document).ready(function () {    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        table = $('#table').DataTable( {            
            ajax: "{{api('PersonType')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'personTypeId'},
                {'data':'personTypeNameTh'},
                {'data':'personGroupNameTh'},
                {'data':'activeFlag',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check text-success\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'personTypeId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:PersonType.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:PersonType.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],                                                        
                },
                {
                    "className": "text-right", "targets": [4],
                },
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: 4 }                                
            ],                
            buttons: [
                {
                    text: "<i class=\"fa fa-plus\"></i>",
                    className: "btn btn-info",
                    action: function (e, dt, node, config) {
                        PersonType.add();
                    }
                },
                {
                    extend: 'print',
                    title: "ประเภทบุคลากร",
                    text: "<i class=\"fa fa-print\"></i> พิมพ์",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "ประเภทบุคลากร",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],     
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            'processing': true,
            'responsive': true
        });       
        frmObj = $('#frmSave').validate();
        
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            var frm = $(this);

            if (frmObj.valid()) {
                MessageConfirm.save(function() {
                    PersonType.save(frm);
                },function() {

                });
            }

        });

        $('#btnCancel').click(function(){           
            PersonType.clear();
        });

        $("#personTypeId").inputmask({
            mask: "9[99]",
            greedy: false 
        });

        $('#personGroupId').select2({
            width:"100%"
        });

        PersonType.loadPersonGroup();
    });

    var PersonType = {
        loadPersonGroup () {
            $('#personGroupId').empty();            

            $.getJSON("{{ api('PersonGroup' )}}", function(data){                         
                $("#personGroupId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#personGroupId").append("<option value=" + item.personGroupId + ">" + item.personGroupNameTh + "</option>"); 
                });
            });
        },
        add: function() {
            PersonType.clear();
            $('a[href="#save"]').tab('show');
            setTimeout(function() {
                $('#personTypeId').select();
            },300);
        },
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#personTypeId').val("");
            frmObj.resetForm();
            $('#personTypeId').focus();            
        },
        edit: function(id) {            
            $.ajax({
                url: "{{api('PersonType/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {                        
                        PersonType.clear();

                        var data = response.data;
                        $('#personTypeId').val(data.personTypeId);
                        $('#personGroupId').val(data.personGroupId).trigger('change');
                        $('#personTypeNameTh').val(data.personTypeNameTh);
                        $('#personTypeNameEn').val(data.personTypeNameEn);
                        $('#remark').val(data.remark);
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        }                        
                        $('a[href="#save"]').tab('show');
                        setTimeout(function() {
                            $('#personGroupId').select();
                        },300);     
                    }
                }
            });
        },
        delete: function(id) {             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            PersonType.clear();
                            table.ajax.reload();

                            $('#personTypeId').focus();
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
            
            
        },
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        PersonType.clear();
                        table.ajax.reload();                        
                        $('#personGroupId').focus();

                        MessageNotify.saveSucess();
                    } else {
                        MessageNotify.error(response);
                    }
                }
            });

        }
    };
    
</script>
@endsection
