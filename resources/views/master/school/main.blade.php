@extends('layout.app')

@section('style')
@include('plugin.datatable.css')
@endsection

@section('page-header')
<div class="page-header-title">
    <h5 class="m-b-10">โรงเรียน</h5>
</div>
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-home"></i></a></li>
    <li class="breadcrumb-item"><a href="#!">ข้อมูลพื้นฐานระบบนิเทศ</a></li>
    <li class="breadcrumb-item"><a href="#!">โรงเรียน</a></li>
</ul>
@endsection

@section('content')
<ul class="nav nav-tabs" id="tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="search-tab" data-toggle="tab" href="#search" role="tab" aria-controls="search" aria-selected="true"><i class="fa fa-search"></i> ค้นหา</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="save-tab" data-toggle="tab" href="#save" role="tab" aria-controls="save" aria-selected="false"><i class="fa fa-save"></i> บันทึก</a>
    </li>    
</ul>
<div class="tab-content" id="tabContent">
    <div class="tab-pane fade show active" id="search" role="tabpanel" aria-labelledby="search-tab">
        @include('master.school.search')
    </div>
    <div class="tab-pane fade" id="save" role="tabpanel" aria-labelledby="save-tab">
        @include('master.school.save')
    </div>    
</div>
@endsection


@section('script') 
@include('plugin.datatable.js')
<script>   
    var table;
    var frmObj;
    $(document).ready(function () {    
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        table = $('#table').DataTable( {            
            ajax: "{{api('School')}}",
            type:'GET',
            dataType:'json',
            columns:[
                {'data':'schoolId'},
                {'data':'schoolNameTh'},
                {'data':'provinceName'},
                {'data':'activeFlag',render: function(data) {
                    if (data=="Y") {
                        return "<i class=\"fa fa-check text-success\"></i>";
                    } else {
                        return "";
                    }
                }},                
                {'data':'schoolId' , render: function(data){
                    return "<a class=\"btn btn-sm btn-primary\" href=\"javascript:School.edit('" + data + "');\"><i class=\"fa fa-edit\"></i> แก้ไข</a> " +
                    "<a class=\"btn btn-sm btn-danger\" href=\"javascript:School.delete('" + data + "');\"><i class=\"fa fa-trash\"></i> ลบ</a>";
                }}
            ],      
            columnDefs: [
                {
                    "className": "text-center", "targets": [0,3],                                                        
                },
                {
                    "className": "text-right", "targets": [4],
                },
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: 4 }                                
            ],                
            buttons: [
                {
                    text: "<i class=\"fa fa-plus\"></i>",
                    className: "btn btn-info",
                    action: function (e, dt, node, config) {
                        School.add();
                    }
                },
                {
                    extend: 'print',
                    title: "โรงเรียน",
                    text: "<i class=\"fa fa-print\"></i> พิมพ์",
                    className: "btn btn-primary",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
                {
                    extend: 'excel',
                    title: "โรงเรียน",
                    text: "<i class=\"fa fa-file-excel\"></i> Excel",
                    className: "btn btn-success",
                    exportOptions: {
                        modifier: {
                            order: 'index', // 'current', 'applied',                                
                            page: 'all', // 'all', 'current'
                            search: 'none' // 'none', 'applied', 'removed'
                        }
                    }
                },
            ],     
            "dom": "<'row'<'col-md-6'B><'col-md-6'f>>" +
                "<'row'<'col-md-12'tr>>" +
                "<'row'<'col-md-6'i><'col-md-6'p>>",
            'processing': true,
            'responsive': true
        });       
        frmObj = $('#frmSave').validate();
        
        $('#frmSave').submit(function (e) { 
            e.preventDefault();
            
            var frm = $(this);

            if (frmObj.valid()) {
                MessageConfirm.save(function() {
                    School.save(frm);
                },function() {

                });
            }

        });

        $('#btnCancel').click(function(){           
            School.clear();
        });

        $('#provinceId').select2({
            width:"100%",
        }).on('change',function(e){
            School.loadAmphur($(this).val());            
        });      

        $('#amphurId').select2({
            width:"100%",
        }).on('change',function(e){
            School.loadTambon($('#provinceId').val(),$(this).val());
        });       
        
        $('#districtId').select2({
            width:"100%",
        }).on('change',function(e){
           // School.loadTambon($('#provinceId').val(),$('#amphurId').val(),$(this).val());
        });
        
        
        $('#schoolSubId').select2({
            width:"100%"
        });
        $('#schoolTypeId').select2({
            width:"100%"
        });
        $("#schoolId").inputmask({
            mask: "9[9]",
            greedy: false 
        });

        School.loadSchoolSub();
        School.loadSchoolType();
        School.loadProvince();
       // School.loadAmphur();
        //School.loadTambon();
    });
    
    var dataProvinceId='';
    var dataAmphurId='';
    var dataAmphurName='';
    var dataTambonId='';
    var dataTambonName='';
   

    var School = {
        loadProvince () {
            $('#provinceId').empty();  
            $.getJSON("{{ api('WSSiProvince')}}", function(data){                         
                $("#provinceId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#provinceId").append("<option value=" + item.provinceId + ">" + item.provinceName + "</option>"); 
                });
            });
        },
        loadAmphur(provinceId) {       
            $('#amphurId').empty(); 
            $.getJSON("{{ api('WSSiAmphur/listAmphurByProvince')}}", { provinceId: provinceId })
            .done(function (data){
                $("#amphurId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#amphurId").append("<option value=" + item.amphurId + ">" + item.amphurName + "</option>"); 
                });
                $('#amphurId').val(dataAmphurId).trigger('change');   
            });             
           
        },
        loadTambon(provinceId,amphurId) {   
            $('#districtId').empty(); 
            $.getJSON("{{ api('WSSiTambon/listTambonByAmphur')}}", { provinceId: provinceId,amphurId:amphurId })
            .done(function (data){
                $("#districtId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#districtId").append("<option value=" + item.tambonId + ">" + item.tambonName + "</option>"); 
                });
                $('#districtId').val(dataTambonId).trigger('change');

            }); 
        },
        
        loadSchoolSub () {
            $('#schoolSubId').empty();            

            $.getJSON("{{ api('SchoolSub' )}}", function(data){                         
                $("#schoolSubId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#schoolSubId").append("<option value=" + item.schoolSubId + ">" + item.schoolSubNameTh + "</option>"); 
                });
            });
        },
        loadSchoolType () {
            $('#schoolTypeId').empty();            

            $.getJSON("{{ api('SchoolType' )}}", function(data){                         
                $("#schoolTypeId").append("<option value=''>[เลือกรายการ]</option>");             
                $.each(data.data, function(index,item) {                                    
                    $("#schoolTypeId").append("<option value=" + item.schoolTypeId + ">" + item.schoolTypeNameTh + "</option>"); 
                });
            });
        },
        add: function() {
            School.clear();
            $('a[href="#save"]').tab('show');
            setTimeout(function() {
                $('#schoolId').select();
            },300);
        },
        clear: function () {            
            $('#frmSave')[0].reset();
            $('#provinceId').val('').trigger('change'); 
            $('#schoolId').val("");
            frmObj.resetForm();
            $('#schoolId').focus();            
        },
        edit: function(id) {            
            $.ajax({
                url: "{{api('School/')}}" + id,
                type: "get",                
                dataType: "json",
                success: function (response) {                    
                    if (response.data) {                        
                        School.clear();                        
                        
                        var data = response.data;
                        dataProvinceId = data.provinceId;
                        dataAmphurId = data.amphurId;
                        dataAmphurName = data.amphurName;
                        dataTambonId = data.districtId;
                        dataTambonName = data.tambonName;                        
                        
                        $('#schoolId').val(data.schoolId);
                        $('#schoolSubId').val(data.schoolSubId).trigger('change');
                        $('#schoolTypeId').val(data.schoolTypeId).trigger('change');                        
                        $('#schoolNameTh').val(data.schoolNameTh);
                        $('#schoolNameEn').val(data.schoolNameEn);
                        $('#campusName').val(data.campusName);
                        $('#addressNo').val(data.addressNo);
                        $('#soi').val(data.soi);
                        $('#street').val(data.street);
                        $('#provinceId').val(data.provinceId).trigger('change');                        
                        $('#postcodeId').val(data.postcodeId);
                        $('#addressDesc').val(data.addressDesc);
                        $('#faxNo').val(data.faxNo);
                        $('#telephoneNo').val(data.telephoneNo);
                        $('#mobileNo').val(data.mobileNo);
                        $('#schoolGoogleMap').val(data.schoolGoogleMap);
                        $('#schoolWebsite').val(data.schoolWebsite);
                        $('#schoolEmail').val(data.schoolEmail);
                        $('#schoolFbLink').val(data.schoolFbLink);
                        $('#pictureSchool').val(data.pictureSchool);                        
                        $('#blacklistRemark').val(data.blacklistRemark);
                        //$('#remark').val(data.remark);
                        if (data.fileUrl) {
                            var url = "{{ api('') }}" + data.fileUrl;
                            
                            $('#spnFile').html(
                                "<a class=\"btn btn-sm bg-primary text-white\" href=\"" + url +"\"><i class=\"fa fa-file\"></i> เปิดไฟล์</a>"
                            );
                        }
                                              
                        if (data.activeFlag=="Y") {
                            $('#activeFlag').prop('checked',true);
                        } 
                        if (data.blacklistFlag=="Y") {
                            $('#blacklistFlag').prop('checked',true);
                        }                        
                        $('a[href="#save"]').tab('show');
                        setTimeout(function() {
                            $('#schoolNameTh').select();
                        },300);   
                          
                    }
                }
            });
        },
        delete: function(id) {             
            MessageConfirm.delete(function() {
                $.ajax({
                    type: 'DELETE',
                    url: $('#frmSave').attr('action') + '/' + id,
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            School.clear();
                            table.ajax.reload();

                            $('#schoolId').focus();
                            
                            MessageNotify.deleteSucess();
                        } else {
                            alert(response.message);
                        }
                    }
                });
            },function() {
                
            });
            
            
        },
        save: function(frm) {            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {                    
                    if (response.status) {
                        School.clear();
                        table.ajax.reload();                        
                        $('#schoolNameTh').focus();

                        MessageNotify.saveSucess();
                    } else {
                        MessageNotify.error(response);
                    }
                }
            });

        }
    };
    
</script>
@endsection
