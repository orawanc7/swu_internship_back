<form id="frmSave" action="{{ api('School/save')}}" method="post">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="schoolId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="schoolId" name="schoolId" required autofocus autocomplete="off" maxlength="10">
                </div>                
            </div>            
            <div class="form-group row">
                <label for="schoolNameTh" class="col-md-2 col-form-label mandatory">ชื่อโรงเรียน (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="schoolNameTh" name="schoolNameTh" required autocomplete="off" maxlength="150">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="schoolNameTh" class="col-md-2 col-form-label mandatory">ชื่อโรงเรียน (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="schoolNameEn" name="schoolNameEn" autocomplete="off" maxlength="150">
                </div>                        
            </div>   
            <!--Titapa C. On:27/09/2020-->
            <div class="form-group row">
                <label for="schoolSubId" class="col-md-2 col-form-label mandatory">สังกัดของโรงเรียน</label>
                <div class="col-md-10">                    
                    <select name="schoolSubId" id="schoolSubId" class="form-control select2"></select>
                </div>                        
            </div>   
            <div class="form-group row">
                <label for="schoolTypeId" class="col-md-2 col-form-label mandatory">ประเภทโรงเรียน</label>
                <div class="col-md-10">                   
                    <select name="schoolTypeId" id="schoolTypeId" class="form-control select2"></select>
                </div>                        
            </div>  
            <div class="form-group row">
                <label for="campusName" class="col-md-2 col-form-label">พื้นที่/วิทยาเขต</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="campusName" name="campusName" autocomplete="off" maxlength="50">
                </div>                        
            </div>
            <div class="form-group row">
                <label for="addressNo" class="col-md-2 col-form-label">ที่อยู่</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="addressNo" name="addressNo" autocomplete="off" maxlength="10">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="soi" class="col-md-2 col-form-label">ซอย</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="soi" name="soi" autocomplete="off" maxlength="80">
                </div>                        
            </div>  
            <div class="form-group row">
                <label for="street" class="col-md-2 col-form-label">ถนน</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="street" name="street" autocomplete="off" maxlength="80">
                </div>                        
            </div> 
            <!--End--> 
            <div class="form-group row">
                <label for="provinceId" class="col-md-2 col-form-label mandatory">จังหวัด</label>
                <div class="col-md-10">
                    <select name="provinceId" id="provinceId" class="form-control select2"></select>
                    
                </div>
            </div>     
            <!--Titapa C. On:27/09/2020-->  
            <div class="form-group row">
                <label for="amphurId" class="col-md-2 col-form-label mandatory">อำเภอ</label>
                <div class="col-md-10">
                    <select name="amphurId" id="amphurId" class="form-control select2"></select>
                </div>
            </div> 
            <div class="form-group row">
                <label for="districtId" class="col-md-2 col-form-label mandatory">ตำบล</label>
                <div class="col-md-10">
                    <select name="districtId" id="districtId" class="form-control select2"></select>
                </div>
            </div> 
            <div class="form-group row">
                <label for="postcodeId" class="col-md-2 col-form-label">รหัสไปรษณีย์</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="postcodeId" name="postcodeId" autocomplete="off" maxlength="5">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="addressDesc" class="col-md-2 col-form-label">รายละเอียดโรงเรียน</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="addressDesc" name="addressDesc" autocomplete="off" maxlength="4000">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="faxNo" class="col-md-2 col-form-label">โทรสาร</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="faxNo" name="faxNo" autocomplete="off" maxlength="50">
                </div>                        
            </div>  
            <div class="form-group row">
                <label for="telephoneNo" class="col-md-2 col-form-label">โทรศัพท์</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="telephoneNo" name="telephoneNo" autocomplete="off" maxlength="50">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="mobileNo" class="col-md-2 col-form-label">โทรศัพท์มือถือ</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="mobileNo" name="mobileNo" autocomplete="off" maxlength="50">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="schoolGoogleMap" class="col-md-2 col-form-label">Link แผนที่ จาก Google</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="schoolGoogleMap" name="schoolGoogleMap" autocomplete="off" maxlength="250">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="schoolWebsite" class="col-md-2 col-form-label">เว็บไซต์โรงเรียน</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="schoolWebsite" name="schoolWebsite" autocomplete="off" maxlength="4000">
                </div>                        
            </div> 
            <div class="form-group row">
                <label for="schoolEmail" class="col-md-2 col-form-label">อีเมลโรงเรียน</label>
                <div class="col-md-10">                    
                    <input multiple class="form-control" type="email" id="schoolEmail" name="schoolEmail" autocomplete="off" maxlength="50">
                </div>                        
            </div>
            <div class="form-group row">
                <label for="schoolFbLink" class="col-md-2 col-form-label">ลิงก์ Facebook โรงเรียน</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="schoolFbLink" name="schoolFbLink" autocomplete="off" maxlength="4000">
                </div>                        
            </div> 
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="blacklistFlag"  name="blacklistFlag" type="checkbox" value="Y">
                    <label for="blacklistFlag" class="text-primary">
                            ติดสถานะ Blaclist
                    </label>
                </div>
            </div>
            <div class="form-group row">
                <label for="blacklistRemark" class="col-md-2 col-form-label">หมายเหตุการติด Blaclist</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="blacklistRemark" name="blacklistRemark" autocomplete="off" maxlength="4000">
                </div>                        
            </div> 
            <div class="form-group row" id="fileUpload">
                <label for="docDownloadNameTh" class="col-md-2 col-form-label">ไฟล์เอกสาร</label>
                <div class="col-md-10">
                    <span id="spnFile"></span><input name="file" type="file" id="file"/>
                </div>                        
            </div>     
            <!--End-->    
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะใช้งาน
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>