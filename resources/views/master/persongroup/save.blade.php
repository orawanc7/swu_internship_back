<form id="frmSave" action="{{ api('PersonGroup/save')}}" method="post">
    <div class="card">
        <div class="card-body">
            <div class="form-group row">
                <label for="personGroupId" class="col-md-2 col-form-label mandatory">รหัส</label>
                <div class="col-md-4">
                    <input class="form-control" type="text" id="personGroupId" name="personGroupId" required autofocus autocomplete="off">
                </div>
            </div>
            <div class="form-group row">
                <label for="personGroupNameTh" class="col-md-2 col-form-label mandatory">ชื่อกลุ่มบุคลากร (ไทย)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="personGroupNameTh" name="personGroupNameTh" required autocomplete="off">
                </div>                        
            </div>       
            <div class="form-group row">
                <label for="personGroupNameEn" class="col-md-2 col-form-label mandatory">ชื่อกลุ่มบุคลากร (อังกฤษ)</label>
                <div class="col-md-10">
                    <input class="form-control" type="text" id="personGroupNameEn" name="personGroupNameEn" autocomplete="off">
                </div>                        
            </div>             

            <div class="form-group row">
                <label for="remark" class="col-md-2 col-form-label">หมายเหตุ</label>
                <div class="col-md-10">
                    <textarea name="remark" id="remark" rows="5" class="form-control"></textarea>
                </div>                        
            </div>               
            <div class="form-group row">
                <div class="col-md-10 offset-md-2 checkbox-color checkbox-primary">
                    <input id="activeFlag"  name="activeFlag" type="checkbox" value="Y">
                    <label for="activeFlag" class="text-primary">
                            สถานะ
                    </label>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button class="btn btn-sm btn-success"><i class="fa fa-save"></i> บันทึก</button>&nbsp;
            <button class="btn btn-sm btn-secondary" id="btnCancel" type="button"><i class="fa fa-undo"></i> ยกเลิก</button>
        </div>
    </div>     
</form>