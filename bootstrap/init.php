<?

/**
 * Session Start
 */
if (!isset($_SESSION)) session_start();

/**
 * Init
 */


/**
 * Global Constant
 */
define('BASE_PATH', realpath(__DIR__ . '/../'));
define('WWW_PATH', realpath(__DIR__ . '/../') . "/wwwroot/");

date_default_timezone_set("Asia/Bangkok");

/**
 * Autoload
 */

$loader  = require_once BASE_PATH . '/vendor/autoload.php';
//$loader->addPsr4('App\\', BASE_PATH . "/app");
/**
 * Load Environment
 */
new  \App\Classes\Env(BASE_PATH);

/**
 * Initial Routing
 */
$router = new AltoRouter();
$router->setBasePath(getenv('APP_PATH'));
require_once BASE_PATH . '/App/Routing/web.php';
require_once BASE_PATH . '/App/Routing/base.php';

//print_r($router->match());

new \App\Classes\RoutingDispatcher($router);
