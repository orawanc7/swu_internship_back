var MessageNotify = {
    saveSucess: function () {
        new PNotify({
            title: 'Sucess',
            text: 'บันทึกข้อมูลเรียบร้อยแล้ว',
            type: 'success',
            delay: 300,
        });
    },
    deleteSucess: function () {
        new PNotify({
            title: 'Sucess',
            text: 'ลบข้อมูลเรียบร้อยแล้ว',
            type: 'success',
            delay: 300,
        });
    },
    error: function (msg) {
        new PNotify({
            title: 'Error',
            text: msg,
            type: 'error',
            delay: 300,
        });
    },
}

var MessageConfirm = {
    save: function (success, cancel) {
        swal({
            title: "Swu-Internship",
            text: "กรุณายืนยันการบันทึกข้อมูล",
            icon: "info",
            buttons: true,
            dangerMode: false,
        }).then(function (willSave) {
            if (willSave) {
                if (success) {
                    success();
                }
            } else {
                if (cancel) {
                    cancel();
                }
            }
        });
    },
    delete: function (success, cancel) {
        swal({
            title: "Swu-Internship",
            text: "กรุณายืนยันการลบข้อมูล",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then(function (willDelete) {
            if (willDelete) {
                if (success) {
                    success();
                }
            } else {
                if (cancel) {
                    cancel();
                }
            }
        });
    }
}