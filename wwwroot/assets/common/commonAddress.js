var CommonAddress = {
    loadProvince() {
        $('#provinceId').empty();
        $.getJSON("{{ api('WSSiProvince')}}", function (data) {
            $("#provinceId").append("<option value=''>[เลือกรายการ]</option>");
            $.each(data.data, function (index, item) {
                $("#provinceId").append("<option value=" + item.provinceId + ">" + item.provinceName + "</option>");
            });
        });
    },
    loadAmphur(provinceId) {
        $('#amphurId').empty();
        $.getJSON("{{ api('WSSiAmphur/listAmphurByProvince')}}", { provinceId: provinceId })
            .done(function (data) {
                $("#amphurId").append("<option value=''>[เลือกรายการ]</option>");
                $.each(data.data, function (index, item) {
                    $("#amphurId").append("<option value=" + item.amphurId + ">" + item.amphurName + "</option>");
                });
                $('#amphurId').val(dataAmphurId).trigger('change');
            });

    },
    loadTambon(provinceId, amphurId) {
        $('#districtId').empty();
        $.getJSON("{{ api('WSSiTambon/listTambonByAmphur')}}", { provinceId: provinceId, amphurId: amphurId })
            .done(function (data) {
                $("#districtId").append("<option value=''>[เลือกรายการ]</option>");
                $.each(data.data, function (index, item) {
                    $("#districtId").append("<option value=" + item.tambonId + ">" + item.tambonName + "</option>");
                });
                $('#districtId').val(dataTambonId).trigger('change');

            });
    }

}