// Write your JavaScript code.
$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "search": "คำค้น",
        "paginate": {
            "previous": "ก่อนหน้า",
            "next": "ถัดไป"
        },
        "lengthMenu": "_MENU_ รายการต่อหน้า",
        "zeroRecords": "ไม่พบข้อมูล",
        "info": "หน้าที่ _PAGE_ จาก _PAGES_ จำนวน _TOTAL_ รายการ",
        "infoEmpty": "",
        "infoFiltered": "(_MAX_ รายการทั้งหมด)",
        "processing": "กำลังประมวลผล...กรุณารอสักครู่",
        "emptyTable": "ไม่พบข้อมูล"
    },
    "aLengthMenu": [[10, 25, 50], [10, 25, 50]],
    "pageLength": 10
});