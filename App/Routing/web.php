<?php

/**
 *  Controller/Action 
 * */

$router->map('GET', '/', 'App\Controllers\IndexController@index', 'index');
$router->map('POST', '/login', 'App\Controllers\LoginController@index', 'login');
$router->map('GET', '/home', 'App\Controllers\HomeController@index', 'home');

$router->map('GET', '/wsmajor', 'App\Controllers\WSMajorController@index', 'wsmajor');
$router->map('GET', '/wsdepartment', 'App\Controllers\WSDepartmentController@index', 'wsdepartment');
$router->map('GET', '/wslevelgroup', 'App\Controllers\WSLevelGroupController@index', 'wslevelgroup');
$router->map('GET', '/wsdegree', 'App\Controllers\WSDegreeController@index', 'wsdegree');
$router->map('GET', '/wsprename', 'App\Controllers\WSPrenameController@index', 'wsprename');
$router->map('GET', '/wssiprovince', 'App\Controllers\WSSiProvinceController@index', 'wssiprovince');
$router->map('GET', '/wssiamphur', 'App\Controllers\WSSiAmphurController@index', 'wssiamphur');
$router->map('GET', '/wssitambon', 'App\Controllers\WSSiTambonController@index', 'wssitambon');
$router->map('GET', '/wssizipcode', 'App\Controllers\WSSiZipcodeController@index', 'wssizipcode');

//Master
$router->map('GET', '/persongroup', 'App\Controllers\PersonGroupController@index', 'persongroup');
$router->map('GET', '/persontype', 'App\Controllers\PersonTypeController@index', 'persontype');
$router->map('GET', '/school', 'App\Controllers\SchoolController@index', 'school');

//Doc Download
$router->map('GET', '/docgroupdownload', 'App\Controllers\DocGroupDownloadController@index', 'docgroupdownload');
$router->map('GET', '/docdownload', 'App\Controllers\DocDownloadController@index', 'docdownload');

//Document
$router->map('GET', '/documenttype', 'App\Controllers\DocumentTypeController@index', 'documenttype');
$router->map('GET', '/document', 'App\Controllers\DocumentController@index', 'document');
$router->map('GET', '/form', 'App\Controllers\FormController@index', 'form');
$router->map('GET', '/form/design/[a:frmHdrId]', 'App\Controllers\FormController@design', 'form.design');

//Preprocess
$router->map('GET', '/schdround', 'App\Controllers\SchdRoundController@index', 'schdround');
$router->map('GET', '/schdschool/[a:roundId]', 'App\Controllers\SchdSchoolController@index', 'schdschool');
$router->map('GET', '/schdactivity/[a:roundId]/[a:courseCd]', 'App\Controllers\SchdActivityController@index', 'schdactivity');

