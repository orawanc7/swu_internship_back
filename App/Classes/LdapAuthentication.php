<?php

namespace App\Classes;

class LdapAuthentication
{
    protected $host;
    protected $basedn;
    protected $port;

    public function __construct()
    {
        $this->host = getenv('LDAP_HOST');
        $this->basedn = getenv('LDAP_BASEDN');
        $this->port = getenv('LDAP_PORT');
    }

    public function login($user, $password)
    {
        $bindDN = 'uid=' . $user . ',' . $this->basedn;

        try {
            $ldap = ldap_connect($this->host, $this->port);
            if ($ldap !== FALSE) {
                ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
                ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

                $bind = ldap_bind($ldap, $bindDN, $password);

                if ($bind === FALSE) {
                    ldap_close($ldap);
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } catch (Exception  $ex) {
            throw $ex;
        }
    }
}
