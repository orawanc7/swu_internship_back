<?php
namespace App\Classes;

trait DateUtil
{
    public function toDate($date)
    {
        $data = trim($date);
        $arr = explode('/', $date);
        return $arr[2]  . "/" . $arr[1] . "/" . $arr[0];
    }

    public function toTime($time)
    {
        $dateTime = date("Y/m/d") . $time;
        return $dateTime;
    }
}
