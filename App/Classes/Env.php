<?php

namespace App\Classes;

class Env {

    public function __construct($p_base_path) {
        $dotEnv = new \Dotenv\Dotenv($p_base_path);
        $dotEnv->load();
    }
}