<?php

namespace App\Classes;

use App\Entities\ItsKeyConfig;
use App\Entities\ItsKeyLastnumber;
use Doctrine\ORM\Id\AbstractIdGenerator;

class KeyGenerator extends AbstractIdGenerator
{
    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
        $keyCd = $entity->getKeyCd();
        $keyGen = $entity->getKeyGen();

        $repConfig = $em->getRepository(ItsKeyConfig::class);
        $keyConfig = $repConfig->findOneBy(
            [
                'keyCd'=> $keyCd
            ]
        );

        $repLast = $em->getRepository(ItsKeyLastnumber::class);
        $key = $repLast->findOneBy(
            [
                'keyCd'=> $keyCd,
                'keyGen'=> $keyGen
            ]
        );
        
        $lastNo = 1;
        
        
        if ($key!=null) {                 
            $lastNo = $key->getLastNo();

            $lastNo += 1;

            $key->setLastNo($lastNo);

            $em->merge($key);
            $em->flush();        
        } else {        
            $key = new ItsKeyLastnumber();
            $key->setKeyCd($keyConfig);
            $key->setKeyGen($keyGen);
            $key->setLastNo($lastNo);

            $em->persist($key);
            $em->flush();      
        }

        if ($keyConfig->getKeyGenFlag()=="Y") {
            $runningNumber = $keyConfig->getRunningNumber();

            $lastNoFormat = str_pad($lastNo,$runningNumber,"0",STR_PAD_LEFT);
            
            return $keyGen . $lastNoFormat;
        } else {
            return $lastNo;
        }
                
    }
}