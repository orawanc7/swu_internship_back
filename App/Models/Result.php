<?php

namespace App\Models;

class Result
{
    private $totals;
    private $filters;
    private $data;
    private $status;
    private $message;

    public function __construct($data,$totals) {
        $this->data = $data;
        $this->totals = $totals;
        $this->filters = count($this->data);
    }

    public function getJsonResult() {
        header('Content-Type: application/json;charset=utf-8');  

        $alldata = array (
            'recordsTotal' => $this->totals,
            'recordsFiltered' => $this->totals,
            'data' => $this->data
        );

        echo json_encode($alldata);   
    }

    public function getResult() {

    }
}
