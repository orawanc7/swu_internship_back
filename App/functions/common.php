<?php

use eftec\bladeone;
use eftec\bladeone\BladeOneHtmlBootstrap;

class myBlade extends  bladeone\BladeOne {
    use bladeone\BladeOneHtmlBootstrap;
}

function view($path, array $data = [])
{
    $view = __DIR__ . '/../../resources/views';
    $cache = __DIR__ . '/../../bootstrap/cache';
    
    $blade =  new myBlade($view,$cache);
    $blade->setBaseUrl(getenv('APP_URL'));     

    $blade->directive('userimagepath', function ($id) {
        return "<?php echo $id; ?>";
    });
    
    /*
    $blade->directiveRT('datetime', function ($format) {
        echo date($format);
    });    
    

    $blade->directive('datetime', function ($format) {
        return "<?php echo date(" . $format . "); ?>";
    });
    */
    
    echo $blade->run($path,$data);    
}

function api ($url) {
    $strAppPath = getenv('API_URL');

    $strLastChr = substr($strAppPath,strlen($strAppPath),1);

        
    if ($strLastChr != "/") {
        $strAppPath .= "/";
    }    

    return  $strAppPath . $url;
}

function route($url)
{
    $strAppPath = getenv('APP_PATH');

    $strLastChr = substr($strAppPath,strlen($strAppPath),1);
        
    if ($strLastChr != "/") {
        $strAppPath .= "/";
    }

    return  $strAppPath . $url;
}

function icon($id)
{        
    $path = WWW_PATH . "images/users/icons/" . $id . ".png";

    if (file_exists($path)) {
        return route('images/users/icons/' . $id . '.png');        
    }

    return route('images/users/icons/blank.png');    
}

function profile($id)
{    
    $path = WWW_PATH . 'images/users/profiles/' . $id . '.png';

    if (file_exists($path)) {
        return route('images/users/profiles/' . $id . '.png');        
    }

    return route('images/users/profiles/blank.png');    
}