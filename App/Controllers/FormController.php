<?php

namespace App\Controllers;

class FormController extends BaseController {
    public function index() {
        view('document.form.main');        
    }

    public function design($param) {
        $id = $param['frmHdrId'];

        view('document.form.design',[
            'frmHdrId' => $id
        ]);        
    }
}