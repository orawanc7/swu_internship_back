<?php

namespace App\Controllers;

class SchdSchoolController extends BaseController {
    public function index($param) {        
        $roundId = \htmlspecialchars($param['roundId']);        

        view('preprocess.schdschool.main',[
            'roundId'=>$roundId
        ]);        
    }
}