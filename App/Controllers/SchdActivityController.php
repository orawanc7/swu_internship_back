<?php

namespace App\Controllers;

class SchdActivityController extends BaseController {
    public function index($param) {        
        $roundId = \htmlspecialchars($param['roundId']);
        $courseCd = \htmlspecialchars($param['courseCd']);

        view('preprocess.schdactivity.main',[
            'roundId'=>$roundId,
            'courseCd'=>$courseCd,
        ]);        
    }
}