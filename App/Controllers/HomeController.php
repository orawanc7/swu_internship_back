<?php

namespace App\Controllers;
use App\Repositories\BaseRepository;

class HomeController extends BaseController {
    public function index() {
        view('home');
    }    
}